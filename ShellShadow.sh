#! /bin/bash
#miguel amoros
#funciones 2
#---------------------------------------------
#ShowShadow login, te da un login y te sale la info del shadow
function showShadow(){
	ERR_NARGS=1
	ERR_NOLOGIN=2
	OK=0
	#validar args
	if [ $# -ne 1 ]
	then
		echo "err num args"
		echo "usage $0 login"
		return $ERR_NARGS
	fi

	#miramos que existe el login
	login=$1

	line=$(egrep "^$login:" /etc/shadow 2> /dev/null)

	if [ $? -ne 0 ]
	then
		echo "err login $login no existe"
		return $ERR_NOLOGIN
	fi

	pass=$(echo $line | cut -d: -f2)
	last=$(echo $line | cut -d: -f3)
	min=$(echo $line | cut -d: -f4)
	max=$(echo $line | cut -d: -f5)
	warning=$(echo $line | cut -d: -f6)
	inactivity=$(echo $line | cut -d: -f7)
	expire=$(echo $line | cut -d: -f8)

	echo "LOGIN: $login"
	echo "password: $pass"
	echo "last: $last"
	echo "min: $min"
	echo "max: $max"
	echo "warning: $warning"
	echo "inactivity: $inactivity"
	echo "expire: $expire"

	return $ok
}


#showAllGroup que diga gid(gname) ordenados por gid, diferentes y dentro tabulado los usuarios con ese gid
function ShowAllGroups(){
	OK=0

	#buscamos todos los grupos diferentes
	listgids=$(cut -d: -f4 /etc/passwd | sort -gu)

	#para cada gid decimos gname(gid) y dentro todos los users con ese gid
	for gid in $listgids
	do
		count_users=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | wc -l)

		if [ $count_users -ge 2 ]
		then
			gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)

			echo "GNAME(GID): $gname($gid) ---> users: $count_users"
			egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | sort | sed -r 's/(^.*$)/\t\1/g'
		fi

	done

	return $OK
}


# showAllShells
#Llistar totes els shells del sistema per ordre alfabètic i per a cada shell llistar els usuaris que l̉utilitzen. De cada usuari mostrar el login, uid, gid i home. Els usuaris es llisten per ordre de uid.
function showAllShells(){
	OK=0
	#buscamos todos los shells unicos en el etc/passwd
	listshells=$(cut -d: -f7 /etc/passwd | sort -u 2> /dev/null)

	#para cada shell buscamos los usuarios
	for shell in $listshells
	do
		count_users=$(egrep ":$shell$" /etc/passwd | wc -l)
		if [ $count_users -ge 2 ]
		then
			echo "$shell ---> users: $count_users"
			egrep ":$shell$" /etc/passwd | sort -t: -k3g,3 | cut -d: -f1,3,4,6 | sed -r 's/(^.*$)/\t\1/g'
			fi
	done

	return $OK
}
