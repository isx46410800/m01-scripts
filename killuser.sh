#! /bin/bash
#Miguel Amoros @j02 
#Exercici killuser.sh
#Eliminar usuario / copia del home del usuario / copia del mail user / llistar los files del usuario en el sistema / listar procesos usuario / treballs de impresion
#-----------------------------------------------

function killuser(){
	ERR_NAGS=1
	ERR_LOGIN=2
	OK=0

	#validamos que haya un argumento
	if [ $# -ne 1 ]
	then
		echo "ERROR: num args incorrecte"
		echo "USAGE: $0 user"
		exit $ERR_NARGS
	fi

	#Validamos que exista ese usuario en el sistema
	login=$1

	dataUser=''
	dataUser$(egrep "^$login:" /etc/passwd 2> /dev/null) #si no la utilizaramos, seria no metida en variable y &> /dev/null

	#si no es not null
	if [ -z "$dataUser" ]
	then
		echo "ERROR: el user $login no existeix"
		exit $ERR_LOGIN
	fi

	#mostramos las tareas a hacer con el usuario indicado
	echo $login
	
	
	#1)hacemos un backup del home del usuario y el mailbox indicando el nombre de archivo y directorio y donde guardarlo
	homeDir=$(echo $dataUser | cut -d: -f6) #~$login no funciona las tildes para el home de usuario

	tar -zcvf /tmp/prova/backuphome_$login.targz $homeDir /var/spool/mail/$login     #No hace falta -C /tmp/prova/ al ponerlo en el nombre

	#eliminamos el direcorio despues de hacer el backup
	echo "rm -rf $homeDir"
	echo "rm /var/spool/mail/$login"


	#2)vemos los procesos del sistema de este usuario
	ps -u $login

	#3)buscamos todos los ficheros de este usuario en el sitema
	echo "find / -user $login -print 2> /dev/null"
	
	#4)Ver los trabajos de impresion
	lpq -U $login
	lprm -U $login

	#6)borramos el usuario
	echo "userdel $login"

	#at y los cron: eliminarlos
	atq -q $login
	crontab -u $login -l
	crontab -u $login -r

	exit $OK
}
