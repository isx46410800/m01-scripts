#! /bin/bash
#scritps funcions
#---------------------

#Fes les següents funcions:


#(1)fsize
#Donat un login calcular amb du l'ocupació del home de l'usuari. Cal obtenir el home del /etc/passwd.

function fsize(){
	if [ $# -ne 1 ]
	then
		echo "err"
		return 1
	fi

	login=$1

	home=$(egrep "^$login:" /etc/passwd | cut -d: -f6)

	if [ -z "$home" ]
	then 
		echo "$login inccorrecte"
	else

		du -h $home
	fi
}


#(2)loginargs
#Aquesta funció rep logins i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsize.
#Verificar que es rep almenys un argument. Per a cada argument verificar si és un login vàlid, si no
#generra una traça d'error.

function loginargs(){
	if [ $# -lt 1 ]
	then
		echo "err args"
		return 1
	fi

	for login in $*
	do
		fsize $login

	done
}


##3)loginfile
#Rep com a argument un nom de fitxer que conté un lògin per línia. Mostrar l'ocupació de disc de cada
#usuari usant fsize. Verificar que es rep un argument i que és un regular file.

function loginfile(){
	if [ $# -ne 1 ]
        then
                echo "err"
                return 1
        fi

	if ! [ -f "$1" ]
	then
		echo "err ficther"
		return 2
	fi

	while read -r login
	do
		fsize $login

	done < $1

	return 0

}


#(4)loginboth
#Rep com a argument un file o res (en aquest cas es processa stdin). El fitxer o stdin contenen un lògin per línia.
#Mostrar l'ocupació de disc del home de l'usuari. Verificar els arguments rebuts. verificar per cada login
#rebut que és vàlid.

function loginbooth(){
	fileIn=/dev/stdin

	if [ $# -eq 1 ]
	then
		fileIn=$1
	fi

	while read -r login
	do
		fsize $login

	done < $fileIn

	return 0
}

#(5)grepgid
#Donat un GID com a argument, llistar els logins dels usuaris que petanyen a aquest grup com a grup principal.
#Verificar que es rep un argument i que és un GID vàlid.

function grepid(){
	if [ $# -ne 1 ]
        then
                echo "err"
                return 1
        fi

	gid=$1

	egrep "^[^:]*:[^:]*:$gid:" /etc/group &> /dev/null
	if [ $? -ne 0 ]
	then
		echo "no es un gid valid"
	else
		egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1
	fi

	return 0
}

#(6)gidsize
#Donat un GID com a argument, mostrar per a cada usuari que pertany a aquest grup l'ocupació de disc del seu home.
#Verificar que es rep un argument i que és un gID vàlid.

function gidsize(){
	if [ $# -ne 1 ]
        then
                echo "err"
                return 1
        fi

        gid=$1

        egrep "^[^:]*:[^:]*:$gid:" /etc/group &> /dev/null
        if [ $? -ne 0 ]
        then
                echo "no es un gid valid"
        else
		llista_logins=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1)
		for login in $llista_logins
		do
			fsize $login
		done
        fi

        return 0

}


#(7)allgidsize
#Llistar de tots els GID del sistema (en ordre creixent) l'ocupació del home dels usuaris que hi pertanyen.

function allgidsize(){
	llistagids=$(cut -d: -f4 /etc/passwd | sort -gu)

	for gid in $llistagids
	do
		gidsize $gid
	done

	return 0
}

#(8)allgroupsize
#Llistar totes les línies de /etc/group i per cada llínia llistar l'ocupació del home dels usuaris que hi pertanyen.
#Ampliar filtrant només els grups del 0 al 100.

function allgroupsize(){
	llistagids=$(cut -d: -f3 /etc/group | sort -gu)

        for gid in $llistagids
        do
		if [ $gid -ge 0 -a $gid -le 100 ]
		then
			egrep "^[^:]*:[^:]*:$gid:" /etc/group
			gidsize $gid		
                fi
        done

        return 0
}

#(9)fstype Donat un fstype llista el device i el mountpoint (per odre de device) de les entrades de fstab d'quest fstype.

#(10)allfstype LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.

#(11)allfstypeif LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint. Es rep un valor numèric d'argument que indica el numéro mínim d'entrades d'aquest fstype que hi ha d'haver per sortir al llistat.
