#! /bin/bash
#Miguel amoros
#scripts de disc
#-----------------------------------------------------
# fsize Donat un login calcular amb du l'ocupació del home de l'usuari. Cal obtenir el home del /etc/passwd. Validar que es rep un argument i que aquest és un lògin vàlid.

function fsize(){
	login=$1
	home=$(egrep "^$login:" /etc/passwd | cut -d: -f6)
	if [ -z "$home" ]
	then
		echo "err: $login no existeix" >> /dev/stderr
	else
		du -h $home
	fi

	return 0
}

# loginargs Aquesta funció rep logins i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsize. Verificar que es rep almenys un argument. Per a cada argument verificar si és un login vàlid, si no generra una traça d'error.

function loginargs(){
	logins=$*
	for usuari in $logins
	do
		fsize $usuari
	done

	return 0
}

# loginfile Rep com a argument un nom de fitxer que conté un lògin per línia. Mostrar l'ocupació de disc de cada usuari usant fsize. Verificar que es rep un argument i que és un regular file.

function loginfile(){
	fileIn=$1

	while read -r line
	do
		fsize $line

	done < $fileIn

	return 0
}


#+ loginboth Rep com a argument un file o res (en aquest cas es processa stdin). El fitxer o stdin contenen un lògin per línia. Mostrar l'ocupació de disc del home de l'usuari. Verificar els arguments rebuts. verificar per cada login rebut que és vàlid.

function loginboth(){
	fileIn=/dev/stdin
	if [ $# -eq 1 ]
	then
		fileIn=$1
	fi

	while read -r login
	do
		fsize $login

	done < $fileIn
}
#+ grepgid Donat un GID com a argument, llistar els logins dels usuaris que petanyen a aquest grup com a grup principal. Verificar que es rep un argument i que és un GID vàlid.

function grepgid(){
	if [ $# -lt 1 ]
	then
		return 1
	fi

	gid=$1

	egrep "^[^:]*:[^:]*:$gid:" /etc/group &> /dev/null #para no sacar nada &>
	if [ $? -ne 0 ]
	then
		return 2
	fi

	egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1

}

#+ gidsize Donat un GID com a argument, mostrar per a cada usuari que pertany a aquest grup l'ocupació de disc del seu home. Verificar que es rep un argument i que és un gID vàlid.

function gidsize(){
	gid=$1

	llistalogins=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1 | sort)

	for login in $llistalogins
	do
		fsize $login
	done



}


#+ allgidsize Llistar de tots els GID del sistema (en ordre creixent) l'ocupació del home dels usuaris que hi pertanyen.

function allgidsize(){
	llistagids=$(cut -d: -f4 /etc/passwd | sort -gu) #porque se cogen los usuarios que ya tesrab en el sistema

	for gid in $llistagids
	do
		gidsize $gid
	done
}

#+ allgroupsize Llistar totes les línies de /etc/group i per cada llínia llistar l'ocupació del home dels usuaris que hi pertanyen. Ampliar filtrant només els grups del 0 al 100.

function allgrpupsize(){
	lllistagids=$(cut -d: -f4 /etc/group | sort -gu)

	if [ $llistagids -ge 0 -a $llistagids -le 100 ]
	then
		egrep "^[^:]*:[^:]*:$gid:" /etc/group 2> /dev/null
		gidsize $llistagids
	fi
}


#Exercicis fdisk/blkid/fstab
#Fes les següents funcions:

#+fstype Donat un fstype llista el device i el mountpoint (per odre de device) de les entrades de fstab d'quest fstype.
function fstype(){
	if [ $# -ne 1 ]
	then
		echo "ERR: numero de args incorrecte"
		echo "USAGE: $0 arg"
		return 1
	fi

	fst=$1
	egrep -v "^#" /etc/fstab | tr -s '[:blank:]' '[ ]' | egrep "^[^ ]* [^ ]* $fst " | cut -d' ' -f1,2 | sort
	
	return 0
}

#+allfstype LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.
function allfstype(){
	l_fstypes=$(egrep -v "^#" /etc/fstab | tr -s '[:blank:]' '[ ]' | cut -d' ' -f3 | sort -u)

	for fstype in $l_fstypes 
	do
		echo $fstype:
		#egrep -v "^#" /etc/fstab | tr -s '[:blank:]' '[ ]' | egrep "^[^ ]* [^ ]* $fstype " | cut -d' ' -f1,2 | sed -r 's/(^.*$)/\t\1/g'
		fstype $fstype
	done

	return 0

}

#+allfstypeif LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint. Es rep un valor numèric d'argument que indica el numéro mínim d'entrades d'aquest fstype que hi ha d'haver per sortir al llistat.
function allfstypeif(){
       	if [ $# -ne 1 ]
        then
                echo "ERR: numero de args incorrecte"
                echo "USAGE: $0 arg"
                return 1
        fi


	entradas=$1
	l_fstypes=$(egrep -v "^#" /etc/fstab | tr -s '[:blank:]' '[ ]' | cut -d' ' -f3 | sort -u)

        for fstype in $l_fstypes
        do
		count=$(egrep -v "^#" /etc/fstab | tr -s '[:blank:]' '[ ]' | egrep "^[^ ]* [^ ]* $fstype " | wc -l)

		if [ $count -ge $entradas ]
		then
			echo $fstype:
                	#egrep -v "^#" /etc/fstab | tr -s '[:blank:]' '[ ]' | egrep "^[^ ]* [^ ]* $fstype " | cut -d' ' -f1,2 | sed -r 's/(^.*$)/\t\1/g'
			fstype $fstype
		fi
        done

        return 0

}


