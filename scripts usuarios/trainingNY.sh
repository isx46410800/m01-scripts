#! /bin/bash
#@Miguel Amoros
#funcion base de datos training
#decir la oficina new york con sus empleados y cada pedido de cada empleado de newyork
#-----------------------------
function Training(){
	ERR_NARGS=1
	ERR_NOM=2
	OK=0

	#miramos el numero de argumentos
	if [ $# -ne 1 ]
	then
		echo "ERROR: num args incorrecte"
		echo "USAGE: $0 arg"
		return $ERR_NARGS
	fi
	
	#miramos si existe el nombre de la oficina
	nom_ofi=$1

	line=$(egrep "^[^:]*:$nom_ofi:" oficinas.dat > /dev/null)
	if [ $? -ne 0 ]
	then
		echo "ERROR: el nom de oficina $nom_ofi no existeix"
		return $ERR_NOM
	fi
	
	#miramos cual es el identificador de la oficina
	id_ofi=$(egrep "^[^:]*:$nom_ofi:" oficinas.dat | cut -d: -f1)
	
	#hacemos listado de num_empls de esa oficina
	num_empls=$(egrep "^[^:]*:[^:]*:[^:]*:$id_ofi:" repventas.dat | cut -d: -f1 | sort -g)

	#contamos si hay al menos un num_empl
	count=$(egrep "^[^:]*:[^:]*:[^:]*:$id_ofi:" repventas.dat | wc -l)
	if [ $count -ge 1 ]
        then
		#TITULO DE OFICINA Y NOMBRE
	        echo "$id_ofi: $nom_ofi ---> $count representants"

		for rep in $num_empls
		do
			nombre=$(egrep "^$rep:" repventas.dat | cut -d: -f2)

			#num_empl + nombre
			echo "$rep: $nombre" | sed -r 's/(^.*$)/\t\1/g'

			#buscamos todos los pedidos de cada rep
			egrep "^[^:]*:[^:]*:[^:]*:$rep:" pedidos.dat | cut -d: -f1,4,5,6,7 | sort -t: -k1g,1 | sed -r 's/(^.*$)/\t\1/g' | sed -r 's/(^.*$)/\t\1/g'
		done
	fi

	return $OK
}
