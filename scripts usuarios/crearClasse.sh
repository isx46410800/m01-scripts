#! /bin/bash
#Miguel Amoros
#SCRIPTS
#Programa que rep com a argument el nom de un curs/classe, per exemple hisx1 o carnicers3. Ha de crear el grup (no ha d'existir), el seu home base (/home/nom-del-curs) i 30 usuaris amb noms tipus hisx1-01..hisx1-30.
#----------------------------------------------

#1) validar arg
ERR_NARGS=1
if [ $# -ne 1 ]; then
  echo "Err nargs incorrecte"
  echo "usage: $0 gname"
  exit $ERR_NARGS
fi
#2) existeix login?
ERR_GNAME=2
gname=$1

grep "^$gname:" /etc/group &> /dev/null
if [ $? -eq 0 ]
then
  echo "Err: el gname $gname ja existeix"
  exit $ERR_GNAME
fi

#3)creamos el nuevo grupo
groupadd $gname

#4)creamos 30 usuarios para este grupo principal con el base home indicat
#creamos el home del grupo
mkdir /home/$gname 2> /dev/null
#creamos la lista de usuarios
userslist=$(echo $gname-{01-30})

for user in $userslist
do
	useradd -N -g $gname -b /home/$gname $user
done

exit $OK 
