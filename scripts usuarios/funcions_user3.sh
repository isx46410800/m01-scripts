#! /bin/bash
#@Miguel Amoros
# funcions de usuaris
#----------------------------

# showAllShells
#Llistar totes els shells del sistema per ordre alfabètic i per a cada shell llistar els usuaris que l̉utilitzen. De cada usuari mostrar el login, uid, gid i home. Els usuaris es llisten per ordre de uid.
function showAllShells(){
	ok=0
	ERR_NOSHELL=2

	#miramos los diferentes shells unicos
	shelltypes=$(cut -d: -f7 /etc/passwd | sort -t: -u)

	#iteramos cada shell detectado
	for shell in $shelltypes
	do

		#mostramos
		echo "$shell"
		egrep ".*:$shell$" /etc/passwd | sort -t: -k3g,3 | cut -d: -f1,3,4,6,7
	

	done


	return $ok
}
