#! /bin/bash
#Miguel Amoros
#filtraGrup.sh
#filtra linia a linia el etc/group i mostrar si te usuarios amb aquest grup com principal
#-------------------------------------------------------------------------

#3)filtra linia a linia el etc/group i mostrar si te usuarios amb aquest grup com principal
file=/dev/stdin

#si es un argumento
if [ $# -eq 1 ]
then
	file=$1
fi

#para cada linea del /etc/group
while read -r line
do
	#recortamos el gid del /etc/group
	gid=$(echo $line | cut -d: -f3)

	#lo buscamos en el /etc/passwd
	count=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd &> /dev/null | wc -l)

	#si tiene usuarios, imprimimos linea y los usuarios que pertencecen a ese grupo , ordenado y tabulado
	if [ $count -ge 3 ]
	then
		echo "$line"
		grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | sort | cut -d: -f1,3 | sed -r 's/^(.*)$/\t\1/g'
	fi

done < $file

exit 0


#2)filtra linia a linia el etc/group i mostrar si te usuarios amb aquest grup com principal
file=/dev/stdin

#si es un argumento
if [ $# -eq 1 ]
then
	file=$1
fi

#para cada linea del /etc/group
while read -r line
do
	#recortamos el gid del /etc/group
	gid=$(echo $line | cut -d: -f3)

	#lo buscamos en el /etc/passwd
	grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd &> /dev/null

	#si tiene usuarios, imprimimos linea y los usuarios que pertencecen a ese grupo , ordenado y tabulado
	if [ $? -eq 0 ]
	then
		echo "$line"
		grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | sort | cut -d: -f1,3 | sed -r 's/^(.*)$/\t\1/g'
	fi

done < $file

exit 0

#si es un argumento
if [ $# -eq 1 ]
then
	file=$1
fi

#para cada linea del /etc/group
while read -r line
do
	#recortamos el gid del /etc/group
	gid=$(echo $line | cut -d: -f3)

	#lo buscamos en el /etc/passwd
	grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd &> /dev/null
	#si tiene usuarios, imprimimos linea
	if [ $? -eq 0 ]
	then
		echo "$line"
	fi

done < $file

exit 0
