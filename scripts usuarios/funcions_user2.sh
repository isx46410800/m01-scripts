#! /bin/bash
#@Miguel Amoros
# funcions de usuaris
#----------------------------

# showGroupMainMembers gname - Donat un gname mostrar el llistat dels usuaris que tenen aquest grup. Mostrar el login, uid, dir i shell dels usuaris. Validar que es rep un argument i que el gname és vàlid.
function showGroupMainMembers(){
	
	#validar els arguments
	ERR_NARGS=1
        ERR_GNAME=2
        OK=0
        
        if [ $# -ne 1 ]
        then
               echo "Error:num arg incorrecte"
               echo "Usage: $0 gname"
               return $ERR_NARGS
        fi

	#mirem si existeix
	gname=$1

	line=""
	line=$(egrep "^$gname:" /etc/group 2> /dev/null)
	if [ -z "$line" ]
	then
		echo "ERROR: El $gname no existeix"
		return $ERR_GNAME
	fi

	#mostramos buscando TODAS las lineas que tengan ese GID en el /etc/passwd con GREP
	gid=$(echo $line | cut -d: -f3)
#	egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6,7 (ORIGINAL)
	
	#Ampliar l̉exercici anterior fent modificacions en el format de presentació de les dades:
	#1. Separades per un tabulador (cada camp de un usuari)
	#2. Separat cada camp per dos espais.
	#3. Ordenat per login i separat per dos espais.
	#4. Ordenat per uig, separat per un espai i tot en majúscules.

	egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6,7 | sort -t: -k2g,2 | sed 's/:/  /g' | tr '[a-z]' '[A-Z]' 
	return $OK

}

#showUserIn < fileIn pasamos varios argumentos por STDIN y que salga gname(gid)
function showUserIn(){
	#no hace falta validar argumentos al ser por entrada STDIN
	ERR_NARGS=1
	ERR_LOGIN=2
	status=0

	while read -r login
	do
		line=$(grep "^$login:" /etc/passwd)
	        if [ $? -ne 0 ]
	        then
	              echo "ERR: No existeix $login" >> /dev/stderr
	              echo "USAGE: $0 login"
		      status=$((status+1))
	        else
	 	      #mostrar
	              gid=$(echo $line | cut -d: -f3)
	              groupLine=$(grep "^[^:]*:[^:]*:$gid:" /etc/group 2> /dev/null)
                      gname=$(echo $groupLine | cut -d: -f1)
		      echo "$gname($gid)"

	      fi
	done
	return $status

}


#showUserList login[...] para varios argumentos como logins y que salga gname(gid)
function showUserList(){
	#validar els arguments
	ERR_NARGS=1
        ERR_LOGIN=2
        status=0
        
        if [ $# -lt 1 ]
        then
               echo "Error:num arg incorrecte"
               echo "Usage: $0 login[...]"
               return $ERR_NARGS
        fi

	login_list=$*
	#iterem els arguments
	for login in $login_list
	do
		line=$(grep "^$login:" /etc/passwd)
	        if [ $? -ne 0 ]
	        then
	              echo "ERR: No existeix $login" >> /dev/stderr
	              echo "USAGE: $0 login"
		      status=$((status+1))
	        else
	 	      #mostrar
	              gid=$(echo $line | cut -d: -f3)
	              groupLine=$(grep "^[^:]*:[^:]*:$gid:" /etc/group 2> /dev/null)
                      gname=$(echo $groupLine | cut -d: -f1)
	              echo "$gname($gid)"
	        fi
										    
	done
	return $status

}


