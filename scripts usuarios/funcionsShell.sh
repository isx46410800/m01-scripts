#! /bin/bash
#@Miguel Amoros
# funcions de usuaris
#mostramos los shells diferentes y sus usuarios(es como hacer un group by, se puede hacer con shell y gid)
#----------------------------
#showAllGroup que diga gid(gname) ordenados por gid, diferentes y dentro tabulado los usuarios con ese gid
function ShowAllGroups(){
	#buscamos los diferentes gids del sistema
	llista_gids=$(cut -d: -f4 /etc/passwd | sort -gu)

	#iteramos por cada gid
	for gid in $llista_gids
	do
		#contamos los usuarios que tienen ese gi
		count=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | wc -l)
		
		#si al menos tienen un usuario
		if [ $count -ge 1 ]
		then
			#buscamos el gname de ese gid
		        gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)

			#titulo
			echo "$gid($gname)---------> $count users"
			#usuarios con esos gids, ordenados por login y tabulado
			egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,7 | sort | sed -r 's/(^.*$)/\t\1/g'
		fi
	done

	return 0
}

# showAllShells
function ShowAllShells(){
	#obtenemos todos los shells unicos del sistema del /etc/passwd
	llista_shells=$(cut -d: -f7 /etc/passwd | sort -u)

	#bucle para cada shell
	for shell in $llista_shells
	do
		#contar numero de usuarios
		count=$(egrep ":$shell$" /etc/passwd | wc -l)
		if [ $count -ge 2 ] #solo mostrar cuando hay 2 o mas usuario(seeria como un having)
		then
			#titulo del listado
			echo "$shell-----> $count usuarios"
			#usuarios del sistema con ese shell y recorte de campos y tabulado
			egrep ":$shell$" /etc/passwd | cut -d: -f1,3,4 | sort -t: -k2g,2 | sed -r 's/(^.*$)/\t\1/g'
		fi
	done


	return 0
}
