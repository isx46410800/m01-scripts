#! /bin/bash
#Miguel Amoros
#SCRIPTS
#Programa que rep com a argument el nom de un curs/classe, per exemple hisx1 o carnicers3. Ha de crear el grup (no ha d'existir), el seu home base (/home/nom-del-curs) i 30 usuaris amb noms tipus hisx1-01..hisx1-30.
#----------------------------------------------
status=0

BASEHOME=/home
#1) validar arg
ERR_NARGS=1
if [ $# -ne 1 ]; then
	echo "Err nargs incorrecte"
	echo "usage: $0 gname"
	exit $ERR_NARGS
fi
#2) existeix el grup?
ERR_GRUPEXIST=2
classe=$1

grep "^$classe:" /etc/group &> /dev/null
if [ $? -eq 0 ]
then
       echo "Err: el gname $classe ja existeix"
       exit $ERR_GRUPEXIST
fi

groupadd $classe && echo "Grup $classe created OK"

#3)Creamos directori base
ERR_DIREXIST=3

mkdir /home/$classe &> /dev/null && echo "MKDIR /home/$classe done OK"
if [ $? -ne 0 ]
then
	echo "ERROR: directori $BASEHOME/$classe ja existeix"
	exit $ERR_DIREXIST
fi

#4)Cambiamos los permisos del grupo para que sea de root y del grup creado
chgrp $classe $BASEHOME/$classe && echo "echo chgrp OK"

#5)Creamos los usuarios
llista_users=$(echo $classe-{1,2})
for user in $llista_users
do
	useradd -m -N -g $classe -b /home/$classe $user &> /dev/null && echo "usuario $user creat OK"
	if [ $? -ne 0 ]
	then
		echo "ERR: el $user ja existeix" >> /dev/stderr
		status=4
	fi
done

exit $status

