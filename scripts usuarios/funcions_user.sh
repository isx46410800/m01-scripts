#! /bin/bash
#@Miguel Amoros
# funcions de usuaris
#----------------------------


#ShowUser ha de dir grup: gname(gid)
function ShowUser(){
	 ERR_NARGS=1
         ERR_LOGIN=2
         OK=0
         #validar args
         if [ $# -ne 1 ]
         then
                 echo "Error:num arg incorrecte"
                 echo "Usage: $0 login"
                 return $ERR_NARGS
         fi
         #validar si existeix
         login=$1

         line=""
         line=$(grep "^$login:" /etc/passwd 2> /dev/null)
         if [ -z "$line" ]
         then
                echo "ERR: No existeix $login"
                echo "USAGE: $0 login"
                return $ERR_LOGIN
         fi
         
         #mostrar
         gid=$(echo $line | cut -d: -f3)
         groupLine=$(grep "^[^:]*:[^:]*:$gid:" /etc/group 2> /dev/null)
         gname=$(echo $groupLine | cut -d: -f1)
         
         echo "$gname($gid)"
         return $OK
}

#showGroup(gname)
function showGroup(){
	 ERR_NARGS=1
         ERR_GNAME=2
         OK=0
         #validar args
         if [ $# -ne 1 ]
         then
                 echo "Error:num arg incorrecte"
                 echo "Usage: $0 gname"
                 return $ERR_NARGS
         fi
         #validar si existeix
         gname=$1

         line=""
         line=$(grep "^$gname:" /etc/group 2> /dev/null)
         if [ -z "$line" ]
         then
                echo "ERR: No existeix $gname"
                echo "USAGE: $0 gname"
                return $ERR_GNAME
         fi

	 #mostrar
	 gid=$(echo $line | cut -d: -f3)
	 userlist=$(echo $line | cut -d: -f4)
	
	 echo "gname: $gname"
	 echo "gid: $gid"
	 echo "userlist: $userlist"	

	 return $OK
}

#showUserGecos(login)
function showUserGecos(){
	 ERR_NARGS=1
         ERR_NOLOGIN=2
         OK=0
         #validar args
         if [ $# -ne 1 ]
         then
	         echo "Error:num arg incorrecte"
	         echo "Usage: prog login"
	         return $ERR_NARGS
	 fi

	 #validar si existeix
	 login=$1

	 line=""
	 line=$(grep "^$login:" /etc/passwd 2> /dev/null)

	 if [ -z "$line" ]
         then
                echo "ERR: No existeix $login"
                echo "USAGE: $0 login"
                return $ERR_NOLOGIN
         fi

	#mostrar gecos
	gecos=$(echo $line | cut -d: -f5)
	name=$(echo $gecos | cut -d, -f1)
	office=$(echo $gecos | cut -d, -f2)
	phoneOffice=$(echo $gecos | cut -d, -f3)
	phoneHome=$(echo $gecos | cut -d, -f4)

	echo "gecos: $gecos"
	echo "name: $name"
	echo "office: $office"
	echo "phoneoffice: $phoneOffice"
	echo "phoneHome: $phoneHome"

	return $OK

															 
}




#showuser(login)
#mostrar un a un els camps amb label
function showUser(){
	ERR_NARGS=1
	ERR_NOLOGIN=2
	OK=0

	#validar args
	if [ $# -ne 1 ]
	then
		echo "Error:num arg incorrecte"
		echo "Usage: prog login"
		return $ERR_NARGS
	fi

	#validar si existeix
	login=$1

	line=$(grep "^$login:" /etc/passwd) 2> /dev/null
	
	if [ $? -ne 0 ]
	then
		echo "ERR: No existeix $login"
		echo "USAGE: prog login"
		return $ERR_NOLOGIN
	fi

	#mostrar
	uid=$(echo $line | cut -d: -f3)
	gid=$(echo $line | cut -d: -f4)
	gecos=$(echo $line | cut -d: -f5)
	home=$(echo $line | cut -d: -f6)
	shell=$(echo $line | cut -d: -f7)

	echo "login: $login"
	echo "uid: $uid"
	echo "gid: $gid"
	echo "gecos: $gecos"
	echo "home: $home"
	echo "shell: $shell"

	return $OK

}



#funcio que saluda
function hola(){
	echo "Hola bon dia"
	return 0
}

#suma els dos arguments que rep
function suma(){
	echo $(($1+$2))
	return 0
}
