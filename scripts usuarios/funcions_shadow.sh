#! /bin/bash
#@Miguel Amoros
# funcions de shadow dando un login y te salga la info del shadow
#----------------------------

#ShowShadow login, te da un login y te sale la info del shadow

function showShadow(){
	ok=0
	ERR_NARGS=1
	ERR_LOGIN=2

	#validamos argumentos
	if [ $# -ne 1 ]
        then
		 echo "Error:num arg incorrecte"
                 echo "Usage: $0 login"
                 return $ERR_NARGS
        fi


	#miramos si existe en el sistema este login
	login=$1
	line=""
        line=$(egrep "^$login:" /etc/shadow 2> /dev/null)
        if [ -z "$line" ]
        then
                echo "ERROR: El login $login no existeix"
                return $ERR_LOGIN
        fi

	#mostramos
	passwd=$(echo $line | cut -d: -f2)
	last=$(echo $line | cut -d: -f3)
	min=$(echo $line | cut -d: -f4)
	max=$(echo $line | cut -d: -f5)
	warning=$(echo $line | cut -d: -f6)
	inactivity=$(echo $line | cut -d: -f7)
	expire=$(echo $line | cut -d: -f8)


	echo $login
	#egrep "^$login:" /etc/shadow | sed -r 's/(^.*$)/\t\1/g'
	echo passwd: $passwd | sed -r 's/(^.*$)/\t\1/g'
	echo last: $last | sed -r 's/(^.*$)/\t\1/g'
	echo min age: $min | sed -r 's/(^.*$)/\t\1/g'
	echo max age: $max | sed -r 's/(^.*$)/\t\1/g'
	echo warning: $warning | sed -r 's/(^.*$)/\t\1/g'
	echo inactivity: $inactivity | sed -r 's/(^.*$)/\t\1/g'
	echo expire: $expire | sed -r 's/(^.*$)/\t\1/g'

	return $ok
}
