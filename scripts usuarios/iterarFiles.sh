#! /bin/bash
#Miguel Amoros @j02
#iterar el contingut d'un fitxer per stdin
#-------------------------------------
#lo mismo pero si no hay argumento, que lea la entrada estandard
	#iterar un file  /etc/passwd o por STDIN con un campo gid mayor de 500
file=/dev/stdin #per defecte, processo la entrada standard

#si el numero de argumentos es 1
if [ $# -eq 1 ]
then
	file=$1
fi
while read -r line
do
       gid=$(echo $line | cut -d: -f4)
       if [ $gid -gt 500 ]
       then
	       echo $line | cut -d: -f1,3,4,7
       fi
done < $file


#procesamos un fichero /etc/passwd con un campo mayor de gid 500 y lo mostramos
while read -r line
do
	gid=$(echo $line | cut -d: -f4)
	if [ $gid -gt 500 ]
	then
		echo $line | cut -d: -f1,3,4,7
	fi

done < $1
exit 0

#iterar un file per stdin
cont=1
while read -r line
do
	echo "$cont: $line"
	cont=$((cont+1))
done < $1
exit 0
