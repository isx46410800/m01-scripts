#! /bin/bash
#@Miguel Amoros
# funcions de base de dades
# funcion NewYork --> oficinas --> repventas --> pedidos
#--------------------------------------------------------
function trainings(){
	#lista de oficinas
	llista_oficinas=$(cut -d: -f1 oficinas.dat | sort -g)

	#por cada oficina vemos su representantes y pedidos de casa uno de ellos
	for oficina in $llista_oficinas
	do
		#contamos los num_empl que tiene cada oficina
		count=$(egrep "^[^:]*:[^:]*:[^:]*:$oficina:" repventas.dat | wc -l)

		#si al menos hay un num_empl
		if [ $count -ge 1 ]
		then
			nom_ofi=$(egrep "^$oficina:" oficinas.dat | cut -d: -f2)
			echo "$oficina: $nom_ofi"
			#buscamos los nombres del num_empl de cada oficina
			num_empls=$(egrep "^[^:]*:[^:]*:[^:]*:$oficina:" repventas.dat | cut -d: -f1 | sort -g)

			for rep in $num_empls
			do
				nombre=$(egrep "^$rep:" repventas.dat | cut -d: -f2)
				echo "$rep: $nombre" | sed -r 's/(^.*$)/\t\1/g'
				egrep "^[^:]*:[^:]*:[^:]*:$rep:" pedidos.dat | cut -d: -f1,2,5,6,7 | sed -r 's/(^.*$)/\t\1/g' | sed -r 's/(^.*$)/\t\1/g'
			done
		fi
	done

	return 0
}
