#! /bin/bash
#miguel amoros
#funcions gethome te da un login y te muestra el home del usuari
#------------------

#itera linea a linea el ficher etc/passwd i per a cada usuari, nos dona el seu size, solo cuando el gid es mas grande 500
function getAllSizes(){
	while read -r line
	do
		
		login=$(echo $line | cut -d: -f1)
		Homelogin=$(getHome $line)
		getSize $Homelogin
	done < /etc/passwd
}

#itera linea a linea el ficher etc/passwd i per a cada usuari, nos dona el seu size.
function getAllSizes(){
	while read -r line
	do
		login=$(echo $line | cut -d: -f1)
		Homelogin=$(getHome $line)
		getSize $Homelogin
	done < /etc/passwd
}

#rep logins per stdin i ens diu el size del home de cada uno dels logins
function getSizeIn(){
	while read -r line
	do
		#para calcular el home conseguimos el home con la funcion y le pasamos el argumento
		homeDir=$(getHome $line) 

		#conseguimos el size llamando la funcion con el argumento anterior
		getSize $homeDir
	done
}

#rep com argument un home i hem de dir el size de su directorio en K
function getSize(){
	home=$1
	du -sk $home | cut -f1
	return $?
}


#donada una llista de logins per argument, ens retorna el home del users pero utilizando la funcion anterior
function getHomeArgs(){
	userlists=$*
	for login in $userlists
	do
		getHome $login
	done

}

#funcions gethome te da un login y te muestra el home del usuari
function getHome(){
	login=$1
	grep "^$login:" /etc/passwd | cut -d: -f6 2> /dev/null
	return $?
}
