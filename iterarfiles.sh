#! /bin/bash
#miguel amoros
#funciones iterarfiles
#---------------------------------------------
#le pasamos el etc/group y si tiene mas de 3 usuarios con ese gid en etcpasswrd, imprimos la linea y ademas los users
fileIn=/dev/stdin
if [ $# -eq 1 ]; then
  fileIn=$1
fi
while read -r line
do
  gid=$(echo $line | cut -d: -f3)
  num=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" \
	  /etc/passwd 2> /dev/null | wc -l)
  if [ $num -ge 3 ]; then
    echo $line
    grep "^[^:]*:[^:]*:[^:]*:$gid:" \
      /etc/passwd | sort | cut -d: -f1,3 |  \
      sed -r 's/^(.*)$/\t\1/'
  fi
done < $fileIn
exit 0

#le pasamos el /etc/group y nos imprime la linea y ese gid aparece en el etc/passwd
fileIn=/dev/stdin
if [ $# -eq 1 ]; then
  fileIn=$1
fi
while read -r line
do
  gid=$(echo $line | cut -d: -f3)
  grep "^[^:]*:[^:]*:[^:]*:$gid:" \
      /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo $line
  fi
done < $fileIn
exit 0

#le pasamos el /etc/passwd o linea entera y nos dice si el gid es +500, imprime la linea
file=/dev/stdin
if [ $# -eq 1 ]; then
  file=$1
fi
while read -r line
do
  gid=$(echo $line | cut -d: -f4)
  if [ $gid -gt 500 ]; then
    echo $line | cut -d: -f1,3,4,7
  fi
done < $file
exit 0

#le pasamos el etc/passwd por argumento
while read -r line
do
  gid=$(echo $line | cut -d: -f4)
  if [ $gid -gt 500 ]; then
    echo $line | cut -d: -f1,3,4,7
  fi
done < $1
exit 0

#pasamos datos por teclado
cont=0
while read -r line 
do
  cont=$((cont+1))
  echo "$cont $line"
done

exit 0

#le pasamos el contenido de un fichero por argumento
cont=0
while read -r line 
do
  cont=$((cont+1))
  echo "$cont $line"
done < $1
exit 0 


