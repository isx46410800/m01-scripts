#! /bin/bash
#miguel amoros
#funciones 2
#-----------------------
# showGroupMainMembers gname - Donat un gname mostrar el llistat dels usuaris que tenen aquest grup. Mostrar el login, uid, dir i shell dels usuaris. Validar que es rep un argument i que el gname és vàlid.
function showGroupMainMembers(){
	ERR_NARGS=1
	ERR_NOGNAME=2
	OK=0

	#validar args
	if [ $# -ne 1 ]
	then
		echo "ERR args"
		echo "usage: $0 gname"
		return $ERR_NARGS
	fi

	#validar que existe gname
	gname=$1

	line=$(egrep "^$gname:" /etc/group 2> /dev/null)
	if [ $? -ne 0 ]
	then
		echo "el gname $gname no existe"
		return $ERR_NOGNAME
	fi

	gid=$(echo $line | cut -d: -f3 2> /dev/null)

	echo "GNAME: $gname"

	egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,4,6,7 | sed -r 's/(^.*$)/\t\1/g'

	return $OK

}

#funcion que pase logins por stdin y nos muestre gname(gid)
function showUserIN(){
	status=0

	#pasamos argumentos por stdin
	while read -r login
	do
		
		#validamos que exista el login
		line=$(egrep "^$login:" /etc/passwd 2> /dev/null)
		if [ $? -ne 0 ]
		then
			echo "ERR: el login $login no existe" >> /dev/stderr
			status=$((status+1))
		else
			gid=$(echo $line | cut -d: -f4)
			gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1 2> /dev/null)
			echo "gname(gid): $gname($gid)"
		fi

	done

	return $status
}

#funcion que te diga varios logins y salga su gname(gid) prog login[...]

function showUserList(){
	ERR_NARGS=1
	status=0

	#validamos argumentos
	if [ $# -lt 1 ]
	then
		echo "ERR: num args incorrecte
		echo "USAGE: $0 login[...]
		return $ERR_NARGS
	fi

	userlist=$*

	for login in $userlist
	do
		#validamos que exista el login
		line=$(egrep "^$login:" /etc/passwd 2> /dev/null)
		if [ $? -ne 0 ]
		then
			echo "ERR: el login $login no existe" >> /dev/stderr
			status=$((status+1))
		else
			gid=$(echo $line | cut -d: -f4)
			gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1 2> /dev/null)

			echo "gname(gid): $gname($gid)"
		fi
	done

	return $status
}
