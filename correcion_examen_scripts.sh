#! /bin/bash
#correcion examen
#----------------------------------------------
#4) sophie germain
ERR_NARGS=1
ERR_EXIST=2

#mostramos la ayuda
if [ $# -eq 1 -a "$1" = "-h" ]
then
	echo "mostrrem ayuda"
	exit 0
fi

#miramos el num de arg
if [ $# -lt 2 ]
then
	echo "err nums arg incorrecte"
	exit $ERR_NARGS
fi

#miramos si existe
if [ -e "$1" ] 
then 
	echo "el fitxer ja existeix"
	exit $ERR_EXIST
fi

fileDesti=$1
shift
primers=0
noPrimers=0

for num in $*
do
	bash sophieGermain.sh $num >> /dev/null
	if [ $? -eq 0 ]
	then
		echo $num
		echo "$num" >> $fileDesti
		primers=$((primers+1))
	else
		echo "Error: $num no es un num de sophie" >> /dev/stderr
		noPrimers=$((noPrimers+1))
	fi

done
echo "$primers $noPrimers $((primers+noPrimers))"
exit $ok

#3)fer un programa que rebi els arguments per stdin i mostrar-les per stdout

while read -r line
do
	if [ -f $line ]
	then
		echo "Es un fitxer $line"
		rm -f $line &> /dev/null
		if [ $? -ne 0 ]
		then
			echo "err rm $line" >> /dev/stderr
		fi
	fi
done

exit 0


#2)un programa que aplica un id per cada llista d users
listusers=$*
cont=0

for user in $listusers
do
	id $user &> /dev/null
	if [ $? -eq 0 ]
	then
		echo "el $user existeix"
	else
		echo "el $user no existeix" >> /dev/stderr
		cont=$((cont+1))
	fi
done
echo "hi ha $cont errors"
exit $cont


#1) enumerar lineas

cont=1
while read -r line
do
	echo "$cont: $line"
	cont=$((cont+1))
done
exit 0
