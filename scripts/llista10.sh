#! /bin/bash
#@j02 Miguel Amorós
#10) Programa: prog.sh
#    Rep per stdin GIDs i llista per stdout la informació de cada un d̉aquests grups, en format: gname: GNAME, gid: GID, users: USERS
#wireshark:x:976:guest
#-----------------------------------------------------------------------------------------------------------------------------------------

while read -r gid #(line)
do	
	groupLine=$(grep "^[^:]*:[^:]*:$gid:" /etc/group) #lo que entramos será el gid que será la entrada estandar stdin del fichero etc/group
	#todo esto queda metido en una variable

	gname=$(echo $groupLine | cut -d: -f1) #hacemos un echo del contenido de la variable para poder recortarlo
	
	listusers=$(echo $groupLine | cut -d: -f4)

	echo "gname: $gname, gid: $gid, users: $listusers"
done
exit 0

