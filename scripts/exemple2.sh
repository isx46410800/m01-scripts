#! /bin/bash
#Miguel Amoros
#Exemple-2:
#processar stdin login i mostrar login, uid
#i gid de cada un.
#---------------------------------------------------
status=0

while read -r login
do
	loginLine=$(egrep "^$login:.*" /etc/passwd)
	if [ $? -eq 0 ]
	then
		uid=$(echo $loginLine | cut -d: -f3)
		gid=$(echo $loginLine | cut -d: -f4)

		echo "$login $uid $gid"
	else
		echo "$login es incorrecte" >> /dev/stderr
		status=2
	fi
done
exit $status

