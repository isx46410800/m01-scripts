#! /bin/bash
# Miguel Amoros @j02
#Exercici 18
#prog -a file -b -c -d num -e arg[...]
#-----------------------------------------------------------------

#Mientas haya argumento1, printa el valor $1 y hacemos salto(shift)
opcions=''
arguments=''
fitxer=''
num=''


while [ -n "$1" ]
do
	case $1 in
		'-a')
			fitxer=$2
			shift;;
		'-d')
			num=$2
			shift;;
		'-b'|'-c'|'-e')
			opcions="$opcions $1";;
		*)
			arguments="$arguments $1";;
	esac
	shift
done

echo "las opciones son $opcions"
echo "los argumentos son $arguments"
echo "los ficheros son $fitxer"
echo "los nums son $num"
