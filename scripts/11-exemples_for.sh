#! /bin/bash
#@j02 Miguel Amorós
#exemples for
#
#---------------------------------------------------
#mostrar i enumerar els arguments
cont=0
for arg in $*
do
	cont=$((cont+1)) #porque al enumerarlo empecemos con 1, por eso ponemos antes, operacions amb aritmetic expansion $(())
	echo "$cont: $arg"
done
exit 0


#iterar per la llista de arguments
for arg in $@
do
	echo "$arg"
done
exit 0


#iterar pels elements el resultat de una ordre
llista=$(ls)
for nom in $llista
do
       	echo $nom
done
exit 0



#iterar el contingut d'una variable
llista="pere pau marta ana"
for nom in $llista
do
	echo $nom
done
exit 0

#iterar per una llista de noms
for nom in "pere" "pau" "marta" "ana"
do
	echo $nom
done

