#! /bin/bash
#Miguel Amoros j02
#3. Fer un comptador des de zero fins al valor indicat per l̉argument rebut.
#.......................................................

#iniciamos un contador 
cont=0

#mientras el contador sea mas pequeño o igual que el numero que le pasemos, vamos contando
while [ $cont -le $1 ]
do
	echo $cont
	cont=$((cont+1))
done
exit 0

