#! /bin/bash
#Miguel Amoros
#Exemple-4: processar noms per stdin, si són regular file els anem comptant, si no mostrar
#missatge per stderr. Mostra quants regular files hi ha hagut.
#--------------------------------------------------------------------------------------
status=0
cont=0

while read -r rfile
do
	if [ -f $rfile ]
	then
		cont=$((cont+1))
	else
		echo "ERROR: $rfile no es un regular file" >> /dev/stderr
		status=2
	fi

done
echo "hi ha $cont de regulars file"
exit $status
