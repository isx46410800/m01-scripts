#! /bin/bash
#Miguel Amoros j02
#8. Fer un programa que rep com a argument noms d̉usuari, si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.
#---------------------------------------------------------------------------------------------------------
status=0

#para cada nombre que le pasemos
for nom in $*
do
	#buscamos si esta en el gname de /etc/passwd
	egrep "^$nom:" /etc/passwd > /dev/null
	
	#Si está, pues indicamos que si es, sino no por stderr
	if [ $? -eq 0 ]
	then
		echo "Molt be! $nom es un nom del sistema"
	else
		echo "ERROR: $nom no está en el sistema" >> /dev/stderr
	fi
done

exit $status
