#! /bin /bash
#Miguel Amoros @j02
#3)3) Processar arguments que són matricules:
# a) llistar les vàlides, del tipus: 9999 AAA.
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d’errors (de no vàlides)

#--------------------------------------------------------------------------------------
cont=0

#Procesamos/iteramos cada argumento que pasamos con un bucle for
for argument in $*
do
        formato=$(echo $argument | egrep "^[0-9]{4}-[A-Z]{3}$") #ponemos una variable donde se printe el argumento y se filtre por la matricula

        if [ $? -eq 0 ] # si la condicion de antes se cumple, es decir, status level 0 $?=0, sumalo al contador
        then
                echo "La matricula $argument es correcte"
	else
		echo "ERROR: matricula $argument no correcte!" >> /dev/stderr #passem el error de matricula a STDERROR
		cont=$((cont+1))
        fi
done
echo "Hi ha $cont no valides"
exit $cont

