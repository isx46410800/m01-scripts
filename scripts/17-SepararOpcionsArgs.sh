#! /bin/bash
#@j02 Miguel Amorós
#
#Exercici 16- validar arguments
#prog -h o --help
#prog -a arg
#prog -b -a arg
#cas1 arg // cas2 arg
#----------------------------------------------------------------------------
ERR_NARGS=1
OK=0

#miramos que tenga un argument
if [ $# -lt 1 ]
then
	echo "ERROR: num arguments incorrecte"
	echo "USAGE: prog arg -h / prog -a arg / prog -b -a arg"
	exit $ERR_NARGS
fi

#iterar els arguments de una llista
llista=$*
opcions=''
arguments=''

for element in $llista
do
	#para cada elemento separamos si es opcion o argumento
	case $element in
		'-a'|'-b'|'-c'|'-d'|'-e')
			opcions="$opcions $element";;
		*)
			arguments="$arguments $element";;
	esac
done

#mostra resultats
echo "opcions: $opcions"
echo "arguments: $arguments"
exit 0
