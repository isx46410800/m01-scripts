#! /bin/bash
#@j02 Miguel Amorós
#7) Programa: prog -f|-d arg1 arg2 arg3 arg4
#a) Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
#   Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis.
#   Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2.
#b) Ampliar amb el cas: prog -h|--help.

#---------------------------------------------------
OK=0
ERR_NARGS=1
ERR_ELEM=2



#numero de arguments diferente de 5 error
if [ $# -nq 5 ]
then
	echo "ERROR: Numero de args incorrecte"
	echo "USAGE: Programa: prog -f|-d arg1 arg2 arg3 arg4"
	exit $ERR_NARGS
fi

#si $1 es diferente de -d y -f
if [ "$1" != "-d" -a "$1" != "-f" ]
then
	echo "ERROR: entrada incorrecte"
	echo "USAGE: USAGE: Programa: prog -f|-d arg1 arg2 arg3 arg4"
	exit $ERR_NARGS
fi


#miramos si es help
if [ $# -eq 1 ]
then
       if [ "$1" == "-h" -o "$1" == "--help" ]
       then
             echo "@j02 Miguel Amoros"
             echo "USAGE: Programa: prog -f|-d arg1 arg2 arg3 arg4"
             exit $OK
        else
	     echo "ERROR: format arg incorrecte"
             echo "USAGE: Programa: prog -f|-d arg1 arg2 arg3 arg4"
             exit $ERR_NARGS
      fi
fi

#guardem el la opcion como $1
tipus=$1

#hacemos un shift para solo quedarnos con los argumentos que hemos de pasar y quitar la opcion -d o -f
shift

#bucle para cada iteracion de cada argumento
for arg in $*
do
	if ! [ $tipus "$arg" ] #si el argumento no es $1 que seria como valor de opcion -d/-f, cambiamos el status a 2, lo mismo que if ! [ -f $argument ]
	then
		OK=2
	fi
done
exit $OK

