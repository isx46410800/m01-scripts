#! /bin/bash
#@j02 Miguel Amorós
#12-for-nota.sh nota[...] -> una o mes notes
#Pasar-hi notes i dir si esta suspes o no
#---------------------------------------------------
ERR_NARGS=1
OK=0

#Validamos numero de argumentos
if ! [ $# -ge 1 ]
then
	echo "Error: numero de argumentos incorrecto"
	echo "USAGE: prog nota[...]"
	exit $ERR_NARGS
fi

#Por cada nota que tenemos por argumento
llistanotes=$*

for nota in $llistanotes
do
	if [ $nota -lt 0 -o $nota -gt 10 ]
	then
		echo "ERROR: la nota $nota es incorrecte" >> /dev/stderr #>> porque sino destruye en anterior, asi va añadiendo errores, > es destructivo
		echo "La nota ha de ser de 0 a 10 inclosos" >> /dev/stderr
		echo "USAGE: prog nota[...]" >> /dev/stderr

	elif [ $nota -lt 5 ]
	then
	       echo "$nota: Está suspendido"	
        elif [ $nota -ge 5 -a $nota -lt 7 ]
	then
	       echo "$nota: Está aprobado"
	elif [ $nota -ge 7 -a $nota -lt 9 ]
	then
	       echo "$nota: Aprobado con un notable"
        else
	       echo "$nota: Aprobado con un excelente"
	fi

done

exit $OK
