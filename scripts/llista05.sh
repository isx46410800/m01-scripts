#! /bin/bash
#@j02 Miguel Amorós
#5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#---------------------------------------------------

#per stdin li pasarem un fitxer amb noms per a que processi linia a linia i printi nomes la de menys de 50 caracters
OK=0
while read -r line #leeremos cada linia del fichero y de cada linia la miramos y filtramos si tiene menos de 50 chars
do
	echo $line | egrep "^[a-z]{,49}" > /dev/null
	#chars=$(echo $line | wc -c))
	#if [chars -lt 50]
	if [ $? -eq 0 ]
	then
		echo "Aquesta linia $line te menys de 50 caracters"
	fi
        
done
exit $OK

