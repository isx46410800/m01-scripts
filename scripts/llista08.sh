#! /bin/bash
#@j02 Miguel Amorós
#8) Programa: prog file…
# a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s̉ha comprimit correctament, o un missatge d̉error per stderror si no s̉ha pogut comprimir. En finalitzar es mostra per stdout quants files ha comprimit.
#   Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
#b) Ampliar amb el cas: prog -h|--help.
#---------------------------------------------------------------------------

status=0
ERR_NARGS=1
ERR_ELEM=2

if [ $# -lt 1 ]
then
	echo "ERROR: numero de argumentos incorrecto"
	echo "USAGE: prog file[...]"
	exit $ERR_NARGS
fi

#miramos si es help
if [ $# -eq 1 ]
then
       if [ "$1" == "-h" -o "$1" == "--help" ]
       then
            echo "@j02 Miguel Amoros"
            echo "USAGE: Programa: prog file[...]"
            exit $OK
       else
            echo "ERROR: format arg incorrecte"
            echo "USAGE: Programa: prog file[...]"
            exit $ERR_NARGS
       fi
fi

#para cada file, hay que hacer una accion: comprimir
cont=0

for file in $*
do
	if [ -f $file ] #si el file es un regular file hacemos lo siguiente
	then
		gzip $file &> /dev/null #echo "accion comprimir"
		#si esto se ha hecho bien
		if [ $? -eq 0 ] #ok: nombre del archivo mas contador de archivos comprimidos
		then
			echo $file
			cont=$((cont+1))
		else #no ok: mensaje de error
			echo "Ha anat malamente el $file" >> /dev/stderr #por cada error lo vamos acumulando en los stderror
			status=2 #indicamos que el status de error es 2
		fi
	else #sino es un regular file
		echo "error: no es un file $file"
		status=2
	fi
done
exit $status

