#! /bin/bash
#@j02 Miguel Amorós
#Validar una nota i dir si la nota es aprovat o suspès
#---------------------
#Si el numero de arguments no es 1
if [ $# -ne 1 ]
then
  echo "Error, el numero de arguments es incorrecte"
  echo "USAGE: el programa ha de ser prog arg1(nota)"
  exit 1
fi
#Si la nota no està entre 0-10 inclosos
nota=$1
if [ $nota -lt 0 -o $nota -gt 10 ]
then
  echo "Error, la nota no es vàlida"
  echo "USAGE: la nota ha de ser de 0 a 10 inclosos"
  exit 2
fi


# xixa, si la nota valida es aprovat o suspés
if [ $nota -lt 5 ]
then
  echo "Està suspès" #msg="suspes"
else
 echo "Molt bé! Està aprovat" #msg="aprovat"
fi
#echo $msg
exit 0

