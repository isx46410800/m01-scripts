#! /bin/bash
#@j02 Miguel Amorós
#9) Programa: prog.sh [ -r -m -c cognom  -j  -e edat ]  arg…
#   Escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors corresponents.
#   No cal validar ni mostrar res!
#   Per exemple si es crida: $ prog.sh -e 18 -r -c puig -j wheel postgres ldap
#   retona:     opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap»
#---------------------------------------------------------------------------
status=0
ERR_NARGS=1
ERR_ELEM=2

if [ $# -lt 1 ]
then
	echo "ERROR: numero de argumentos incorrecto"
	echo "USAGE: prog [-n -m -c cognom -j -e edat] nom[...]"
	exit $ERR_NARGS
fi

#miramos si es help
if [ "$1" == "-h" -o "$1" == "--help" ]
then
     echo "@j02 Miguel Amoros"
     echo "USAGE: prog [-n -m -c cognom -j -e edat] nom[...]"
     exit $OK
fi


#bucle para iterar cada opcion y ver cada cosa por separado
cognoms=''
edat=''
opcions=''
arguments=''

#mientras no sea nulo el $1
while [ -n "$1" ]
do
	case $1 in
		'-r'|'-m'|'-j')
			opcions="$opcions $1";;
		'-c')
			cognoms=$2 #vemos que el $2 sera lo que debemos guardarnos
			shift;;
		'-e')
			edat=$2
			shift;;
		*)
			arguments="$arguments $1" #acumulamos en el contador de argumentos cuando son letras
	esac
	shift #ponemos shift para que vaya saltando los argumentos hasta que sea nulo
done

echo "les opcions son $opcions"
echo "els arguments son $arguments"
echo "els cognoms son $cognoms"
echo "les edats son $edat"

exit $status
