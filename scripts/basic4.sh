#! /bin/bash
#Miguel Amoros j02
#4. Fer un programa que rep com a arguments números de més (un o més) i indica per a cada mes rebut quants dies té el més.
#-----------------------------------------------------------------------------------------------------------------------------
status=0

#para cada mes interamos 
for mes in $*
do
	#elegimos que opcion es y su mensaje correspondiente
	case $mes in
		'1'|'3'|'5'|'7'|'8'|'10'|'12')
			echo "El mes $mes tiene 31 dias";;
		'2')
			echo "El mes $mes tiene 28 dias";;
		'4'|'6'|'9'|'11')
			echo "El mes $mes tiene 30 dias";;
		*)
			echo "El mes $mes es incorrecte"
			status=2;;
	esac

done
exit $status
