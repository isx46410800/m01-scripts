#! /bin/bash
#@j02 Miguel Amorós
#Validar si existeix un argument
#pasaria un mes i indicar si el mes té 28/30/31
#---------------------------------------------------

ERR_NAGS=1

#validem numero de arguments
if [ $# -ne 1 ]
then
	echo "Error, numero de arguments incorrecte"
	echo "USAGE: prog mes (un mes del 1 al 12 inclosos)"
	exit $ERR_NARGS
fi

#validar arg 1-12
mes=$1
if [ $mes -lt 1 -o $mes -gt 12 ]
then
	echo "Error, mes no valid"
	echo "USAGE: mes entre 1-12 inclosos"
	echo "USAGE: prog mes"
	echo $ERR_NARGS
fi


#mirem quants dies te el mes
mes=$1

case $mes in
'1'|'3'|'5'|'7'|'8'|'10'|'12')
	echo "El mes $mes tiene 31 días";;
'2')
	echo "El mes $mes tiene 28 días";;
'4'|'6'|'9'|'11')
	echo "El mes $mes tiene 30 días";;
*)
	echo "$mes no es un mes correcto, es otra cosa";;
esac
exit 0
