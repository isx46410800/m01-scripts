#! /bin/bash
#@j02 Miguel Amorós
#exemples while
#
#---------------------------------------------------

#-----------------EXEMPLES PROCESSAR STDIN--------------------------------------------------------------------------------------

#9) mostrar per sortida estandar enumerant les lines i en mayusculas de algo que passem pero STDIN
num=1
while read -r line
do
	echo "$num: $line" | tr '[a-z]' '[A-Z]'
	num=$((num+1))
done
exit 0





#8) iterar fins rebre token "FI"
read -r line
while [ $line != "FI" ]
do
	echo $line
	read -r line
done
exit 0


#7) Mostrar linia a linia la entrada estandar i enumerar cada linea
num=1
while read -r line
do
	echo "$num: $line"
	num=$((num + 1))
done
exit 0

#[isx46410800@j02 m01-scripts]$ bash 13-exemples_while.sh < noms.txt
#[isx46410800@j02 m01-scripts]$ cat noms.txt | bash 13-exemples_while.sh
#[isx46410800@j02 m01-scripts]$ cat noms.txt | bash 13-exemples_while.sh
#1: marta
#32: jordi
#3: ana

#6) Mostrar linia a linia la entrada estandar
while read -r line
do
        echo $line
done
exit 0




#--------------EXEMPLES SHIFT I ARGS---------------------------------------------------------------------------------------------

#5) idem numerant arguments
num=1
while [ -n "$1" ] #mientras no sea null $1, haz desplazamiento shift, habrá tantos $1 como argumentos haya. siempre encapsulado""
do
        echo "$num: $1" #poner listado primero, segundo... mas posicion 1
        num=$((num + 1))	
        shift
done
exit 0


#4)iterar per la llista d'arguments: mostrant-los un a un, operador shift
while [ -n $1 ] #mientras no sea null $1, haz desplazamiento shift, habrá tantos $1 como argumentos haya
do
	echo $1 #para solo printar la posicion 1 de los argumentos
	shift
done
exit 0



#3)exemple SHIFT
while [ $# -gt 0 ] #mientras el numero de argumentas sea mas grande que 0, desplaza
do
	echo "$#: $*" #numero argumentos: listar uno a uno los argumentos
	shift #deplaza
done
exit 0


#------------EXEMPLES GENERALS------------------------------------------------------------------------------------------------------

#2)comptador decramental partint de l'argument rebut [num-0]
num=$1
MIN=0
while [ $num -ge $MIN ] #mientras el numero sea mas grande a 0, vaya haciendo cuenta atras
do
	echo "$num"
	num=$((num - 1)) #no hace falta poner $num ya que el aritmetic lo expande como variable
done
exit 0


#1)mostrar del 0 al 10
MAX=10
num=0

while [ $num -le $MAX ] #menor o igual a 10 para que te printe el 10 incluido, mientras num sea menor que 10, vaya sumando
do
	echo "$num"
	num=$((num + 1))
done
exit 0



