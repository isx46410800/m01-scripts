#! /bin /bash
#Miguel Amoros @j02
#2) Processar els arguments i comptar quantes n̉hi ha de 3 o més caràcters.
#--------------------------------------------------------------------------------------
contador=0

#Procesamos/iteramos cada argumento que pasamos con un bucle for
for argument in $*
do
	formato=$(echo $argument | egrep "^[a-Z]{3,}") #ponemos una variable donde se printe el argumento y se filtre si tiene 3 o mas caracteres
	#echo $argument | egrep '.{3,}' &> /dev/null

	if [ $? -eq 0 ] # si la condicion de antes se cumple, es decir, status level 0 $?=0, sumalo al contador
	then
		contador=$((contador+1))
	fi
done

echo "N'hi ha $contador amb 3 o mes caracters"
exit 0


