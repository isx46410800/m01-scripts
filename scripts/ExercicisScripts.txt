Exercicis Scripts 
@edt ASIX-M01 Curs 2017-2018

-------------------
Processar arguments
-------------------
1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.

#Procesamos/iteramos cada argumento que pasamos con un bucle for
for argument in $*
do
        echo $argument | egrep "^[a-Z]{4,}" # printamos el argumento y filtramos si tiene 4 o mas caracteres
done

exit 0



2) Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.

contador=0

#Procesamos/iteramos cada argumento que pasamos con un bucle for
for argument in $*
do
        formato=$(echo $argument | egrep "^[a-Z]{3,}") #ponemos una variable donde se printe el argumento y se filtre si tiene 3 o mas caracteres

        if [ $? -eq 0 ] # si la condicion de antes se cumple, es decir, status level 0 $?=0, sumalo al contador
        then
                contador=$((contador+1))
        fi
done

echo "N'hi ha $contador amb 3 o mes caracters"
exit 0



3) Processar arguments que són matricules: 
 a) llistar les vàlides, del tipus: 9999 AAA. 
 b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d’errors (de no vàlides)

cont=0

#Procesamos/iteramos cada argumento que pasamos con un bucle for
for argument in $*
do
        formato=$(echo $argument | egrep "^[0-9]{4}-[A-Z]{3}$") #ponemos una variable donde se printe el argumento y se filtre por la matricula

        if [ $? -eq 0 ] # si la condicion de antes se cumple, es decir, status level 0 $?=0, sumalo al contador
        then
                echo "La matricula $argument es correcte"
        else
                echo "ERROR: matricula $argument no correcte!" >> /dev/stderr #passem el error de matricula a STDERROR
                cont=$((cont+1))
        fi
done
echo "Hi ha $cont no valides"
exit $cont




----------------
Processar stdin
----------------

4) Processar stdin cmostrant per stdout les línies numerades i en majúscules..

#per stdin li pasarem un fitxer amb noms per a que processi linia a linia i la indiqui numerada i mayusula per stdout
OK=0
num=1
while read -r line
do
        echo "$num: $line" | tr '[a-z]' '[A-Z]'
        num=$((num+1))
done
exit $OK


[guest@miguel-fedora27 m01-scripts]$ bash llista04.sh < noms.txt 
1: MARTA
2: JORDI
3: ANA
4: MIGUEL



5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters.

#per stdin li pasarem un fitxer amb noms per a que processi linia a linia i printi nomes la de menys de 50 caracters
OK=0
while read -r line #leeremos cada linia del fichero y de cada linia la miramos y filtramos si tiene menos de 50 chars
do
        echo $line | egrep "^[a-z]{,49}" > /dev/null
        #chars=$(echo $line | wc -c))
        #if [chars -lt 50]
        if [ $? -eq 0 ]
        then
                echo "Aquesta linia $line te menys de 50 caracters"
        fi

done
exit $OK




[guest@miguel-fedora27 m01-scripts]$ bash llista05.sh < caracters.txt 
Aquesta linia miguelmiguel te menys de 50 caracters
Aquesta linia 1234567890 te menys de 50 caracters
Aquesta linia 123456789 te menys de 50 caracters



6) Processar per stdin linies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.

#per stdin li pasarem un fitxer amb noms per a que processi linia a linia i printi nomes format retallat
OK=0
while read -r line #leeremos cada linia del fichero 
do
        formato1=$(echo $line | cut -c1)
        formato2=$(echo $line | cut -d' ' -f2)

        echo "$formato1. $formato2" #construimos el formato1 mas el . con el formato2
done
exit $OK


[guest@miguel-fedora27 m01-scripts]$ cat > noms_recortats.txt
Tom Snyder
Miguel Amoros
Cristiano Ronaldo

[guest@miguel-fedora27 m01-scripts]$ bash llista06.sh < noms_recortats.txt 
T. Snyder
M. Amoros
C. Ronaldo


---------------------------
Processar files/directoris
---------------------------

7) Programa: prog -f|-d arg1 arg2 arg3 arg4
 a) Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
    Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis. 
    Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2. 
 b) Ampliar amb el cas: prog -h|--help.

8) Programa: prog file…
   a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s’ha comprimit correctament, o un missatge d’error per stderror si no s’ha pogut comprimir. En finalitzar es mostra per stdout quants files ha comprimit. 
     Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
   b) Ampliar amb el cas: prog -h|--help.

9) Programa: prog.sh [ -r -m -c cognom  -j  -e edat ]  arg…
   Escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors corresponents.
   No cal validar ni mostrar res!
   Per exemple si es crida: $ prog.sh -e 18 -r -c puig -j wheel postgres ldap
   retona:     opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap»

----------------
Processar passwd
----------------

10) Programa: prog.sh 
    Rep per stdin GIDs i llista per stdout la informació de cada un d’aquests grups, en format: gname: GNAME, gid: GID, users: USERS

11) Idem però rep els GIDs com a arguments de la línia d’ordres.

12) Programa -h uid…
    Per a cada uid mostra la informació de l’usuari en format:
    logid(uid) gname home shell

