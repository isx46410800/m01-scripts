#! /bin/bash
#@j02 Miguel Amorós
#Validar si existeix un argument
#mostrar per missatge si es un directori o no
#---------------------------------------------------
ERR_NARGS=1

#miramos que tenga un argument
if [ $# -ne 1 ]
then
        echo "ERROR: num arguments incorrecte"
        echo "USAGE: prog arg1"
        exit $ERR_NARGS
fi

#Si es demana help (-h)
if [ "$1" == "-h" -o "$1" = "--help" ]
then
	echo "@j02 Miguel Amoros"
	echo "USAGE: prog dir"
	exit 0
fi



#xixa
dir=$1

if [ -d $dir ]
then
        echo "$dir Es un directorio"
elif [ -h $dir ]
then
        echo "$dir es un link"
elif [ -f $dir ]
then
	echo "$dir es un regular file"
elif ! [ -e $dir ]
then
	echo "$dir no existeix"
else
        echo "$dir es otra cosa"
fi
exit 0
					
