#! /bin/bash
#Miguel Amoros
#Exemple-1:
#dir si és jove, actiu, jubilat.
#processar n edats rebudes com a arguments.
#--------------------------------------------------------
#codigo de error
status=0


#numero de argumentos correcto
if [ $# -eq 0 ]
then
	echo "ERROR: numero de arguments incorrecte"
	echo "USAGE: prog edat[...]"
	status=1
fi


#xixa
for edat in $*
do
	if [ $edat -lt 18 ]
	then
		echo "$edat es jove"
	elif [ $edat -ge 18 -a $edat -lt 65 ]
	then
		echo "$edat es actiu"
	else
		echo "$edat es jubilat"
	fi
done

exit $status
