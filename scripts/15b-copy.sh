#! /bin/bash
#@j02 Miguel Amorós
#copy.sh
#fa un 'ls' del directori rebut
#copy.sh file[...] directori-desti
#validar args
#validar desti es un dir
#copiar los ficheros que pasemos a un directorio destino
#----------------------------------------------------------------------------
ERR_NARGS=1
ERR_NODIR=2
OK=0

#miramos que tenga un argument
if [ $# -lt 2 ]
then
        echo "ERROR: num arguments incorrecte"
        echo "USAGE: prog file[...] dir-desti"
        exit $ERR_NARGS
fi



#Miramos si es un directori destino el ultimo argumento
	#ejemplo para empezar a probarlo	
	#dirDesti="/tmp"
	#llistafiles="noms.txt carta1.txt carta2.txt"

#Recortamos los campos que necesitamos	
dirDesti=$(echo $* | sed 's/^.* //')
llistafiles=$(echo $* | sed 's/ [^ ]*$//')

if ! [ -d $dirDesti ]
then
	echo "ERROR: no es un directorio"
	echo "USAGE: prog file[...] dir-desti"
	exit $ERR_NODIR
fi

#para cada fichero lo copiamos en el directorio destino
for fitxer in $llistafiles
do
	#si no es un regular file, notificamos error y seguimos
	if ! [ -f $fitxer ]
	then
		echo "ERROR: $fitxer not regular" >> /dev/stderr
	#Si lo es, copiamos cada fichero al directorio destino	
	else
		cp $fitxer $dirDesti
	fi
done
exit $OK

		
	


