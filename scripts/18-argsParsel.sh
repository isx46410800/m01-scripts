#! /bin/bash
#@j02 Miguel Amorós
#
#Exercici 18- separar arguments
#prog [ -a file -b -c -d num -e ] arg[...]
#separar per opciones, file, num y arguments
#
#
#----------------------------------------------------------------------------
ERR_NARGS=1
OK=0

#miramos que tenga un argument
if [ $# -lt 1 ]
then
	echo "ERROR: num arguments incorrecte"
	echo "USAGE: prog arg -h / prog -a arg / prog -b -a arg"
	exit $ERR_NARGS
fi

#iterar els arguments de una llista
llista=$*
opcions=''
arguments=''

for element in $llista
do
	#para cada elemento separamos si es opcion o argumento
	case $element in
		'-a'|'-b'|'-c'|'-d'|'-e')
			opcions="$opcions $element";;
		*)
			arguments="$arguments $element";;
	esac
done

#mostra resultats
echo "opcions: $opcions"
echo "arguments: $arguments"
exit 0
