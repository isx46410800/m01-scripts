#! /bin/bash
# @j02 Miguel Amorós
#29/01/2018
#Validar que té dos arguments i mostrar-los
#------------------------------------
# si num args no es correcte, plegar
if [ $# -ne 2 ]
then
  echo "ERROR: Numero de Arguments incorrecte"
  echo "usage: prog arg1 arg2"
  exit 1
fi
# xixa
echo "Els dos arguments son $1 i $2"
exit 0
