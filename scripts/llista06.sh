#! /bin/bash
#@j02 Miguel Amorós
#6) Processar per stdin linies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.
#---------------------------------------------------

#per stdin li pasarem un fitxer amb noms per a que processi linia a linia i printi nomes format retallat
OK=0
while read -r line #leeremos cada linia del fichero y de cada linia la miramos y filtramos si tiene menos de 50 chars
do
	formato1=$(echo $line | cut -c1)
	formato2=$(echo $line | cut -d' ' -f2)
	#echo $line | sed -r 's/^(.)[^ ]* /\1. /' ##recorta el primer caracter(.) y se expande hasta el espacio mas un espacio y lo otro sin tocar, por lo tanto saldra tamb

	echo "$formato1. $formato2" #construimos el formato1 mas el . con el formato2
done
exit $OK

