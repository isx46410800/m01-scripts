#! /bin/bash
#@j02 Miguel Amorós
#4) Processar stdin cmostrant per stdout les línies numerades i en majúscules..
#
#---------------------------------------------------

#per stdin li pasarem un fitxer amb noms per a que processi linia a linia i la indiqui numerada i mayusula per stdout
OK=0
num=1
while read -r line #leeremos cada linia del fichero
do
        echo "$num: $line" | tr '[a-z]' '[A-Z]' #contamos desde uno, linea y la traducimos a mayusculas
        num=$((num+1))
done
exit $OK

