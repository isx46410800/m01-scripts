#! /bin/bash
#Miguel Amoros j02
#1. Mostrar l̉entrada estàndard numerant línia a línia
#.......................................................

#iniciamos un contador para cada linea de la entrada estandar
cont=1

#leemos linea a linea y la enumeramos con el contador
while read -r line
do
	echo "$cont: $line"
	cont=$((cont+1))
done
exit 0

