#! /bin/bash
#Miguel Amoros j02
#6. Fer un programa que rep com a arguments noms de dies de la setmana i mostra  quants dies eren laborables i quants festius. Si l̉argument no és un dia de la setmana genera un error per stderr.
#   Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
#---------------------------------------------------------------------------------------

status=0
#contadors de laborals y festius
laborals=0
festius=0

#per cada dia que entrem, mirem les opciones i contem.
for dia in $*
do
	case $dia in
		'dilluns'|'dimarts'|'dimecres'|'dijous'|'divendres')
			laborals=$((laborals+1));;
		'dissabte'|'diumenge')
			festius=$((festius+1));;
		*)
			echo "ERROR: $dia no es un dia valid" >> /dev/stderr;;
	esac
done
echo "Hi ha $laborals dies laborals"
echo "Hi ha $festius dies festius"

exit $status
