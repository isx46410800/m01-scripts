#! /bin/bash
#@j02 Miguel Amorós
#llistar dir
#fa un 'ls' del directori rebut
#verificar 1 arg, i que es un dir
#enumerant cada linia una sota el altre
#---------------------------------------------------
ERR_NARGS=1
ERR_NODIR=2

#miramos que tenga un argument
if [ $# -ne 1 ]
then
        echo "ERROR: num arguments incorrecte"
        echo "USAGE: prog arg1"
        exit $ERR_NARGS
fi



#miramos que se aun directorio
dir=$1
if ! [ -d $dir ]
then
	echo "ERROR!! $dir no es un directorio"
	echo "USAGE prog dir"
	exit $ERR_NODIR
fi

#xixa
#iterar pels elements el resultat de una ordre i enumerar-los
llista=$(ls $dir)
cont=1

for element in $llista
do
        echo "$cont: $element"
	cont=$((cont+1))
done
exit 0





