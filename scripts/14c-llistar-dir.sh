#! /bin/bash
#@j02 Miguel Amorós
#llistar dir
#fa un 'ls' del directori rebut
#verificar 1 arg, i que es un dir
#enumerant cada linia una sota el altre
#de cada cosa que imprime decir si es un regular file, dir, link o altre cosa
#----------------------------------------------------------------------------
ERR_NARGS=1
ERR_NODIR=2

#miramos que tenga un argument
if [ $# -ne 1 ]
then
        echo "ERROR: num arguments incorrecte"
        echo "USAGE: prog arg1"
        exit $ERR_NARGS
fi



#miramos que se aun directorio
dir=$1
if ! [ -d $dir ]
then
	echo "ERROR!! $dir no es un directorio"
	echo "USAGE prog dir"
	exit $ERR_NODIR
fi

#xixa
#iterar pels elements el resultat de una ordre i enumerar-los
llista=$(ls $dir)
cont=1

for element in $llista
do
        echo "$cont: $element"
	if [ -d $dir/$element ] #es de podar $dir/ para que pille la ruta absoluta sino no funciona sino es en el directorio activo y los directorios
	then
		echo "Es un directori"
	elif [ -f $dir/$element ]
	then
		echo "Es un regular file"
	elif [ -h $dir/$element ]
	then
		echo "Es un link"
	else
		echo "Es un altre cosa"
	fi
	cont=$((cont+1))
done
exit 0





