#! /bin/bash
#@j02 Miguel Amorós
#10) Programa: prog.sh
#    Rep per stdin GIDs i llista per stdout la informació de cada un d̉aquests grups, en format: gname: GNAME, gid: GID, users: USERS
#wireshark:x:976:guest
#-----------------------------------------------------------------------------------------------------------------------------------------

status=0
#pasaremos gid manualmente ej:50,100.. o por fichero < gid.txt

while read -r gid #(line)
do	
	groupLine=$(grep "^[^:]*:[^:]*:$gid:" /etc/group) #lo que entramos será el gid que será la entrada estandar stdin del fichero etc/group
	#todo esto queda metido en una variable
        if [ $? -ne 0 ]
	then
		echo "Error $gid inexistent" >> /dev/stderr
		status=1
	else

		gname=$(echo $groupLine | cut -d: -f1 | tr '[a-z]' '[A-Z]') #hacemos un echo del contenido de la variable para poder recortarlo
	
		listusers=$(echo $groupLine | cut -d: -f4 | tr '[a-z]' '[A-Z]') #la posem en mayusculas

		echo "gname: $gname, gid: $gid, users: $listusers"
	fi
done
exit $status

