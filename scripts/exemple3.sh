#! /bin/bash
#Miguel Amoros
#Exemple-3: processar gnames rebuts per
#arguments i mostrar per a cada un el llistat
#dels usuaris del passwd que pertanyen a grup.
#---------------------------------------------------------------------
status=0

for gname in $*
do
	#buscamos la linea del gname en el etc/group y recortamos su numero de gid para poder buscarlos en el gid del etc/passwd

	gid=$(egrep "^$gname:" /etc/group | cut -d: -f3)
	
	#buscamos los que tengan ese gid en el campo 4 correspondiente al etc/passwd
	grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd


	if ! [ $? -eq 0 ]
	then
		echo "$gname no es correcte" >> /dev/stderr 
		status=2
	fi
done

exit $status
