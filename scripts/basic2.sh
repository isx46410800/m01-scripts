#! /bin/bash
#Miguel Amoros j02
#2. Mostar els arguments rebuts línia a línia, tot numerànt-los.
#.......................................................

#iniciamos un contador para cada linea de la entrada estandar
cont=1

#iteramos cada argumento y la enumeramos con el contador
for arg in $*
do
	echo "$cont: $arg"
	cont=$((cont+1))
done
exit 0

