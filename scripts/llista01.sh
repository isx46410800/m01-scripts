#! /bin /bash
#Miguel Amoros @j02
#1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#--------------------------------------------------------------------------------------

#Procesamos/iteramos cada argumento que pasamos con un bucle for
for argument in $*
do
	echo $argument | egrep "^[a-Z]{4,}" # printamos el argumento y filtramos si tiene 4 o mas caracteres
	#echo $argument |  egrep '.{4,}'
done

exit 0



