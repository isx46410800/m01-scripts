#! /bin/bash
#@j02 Miguel Amorós
#Validar si existeix un argument
#mostrar per missatge si es un directori o no
#---------------------------------------------------
ERR_NARGS=1

#miramos que tenga un argument
if [ $# -ne 1 ]
then
        echo "ERROR: num arguments incorrecte"
        echo "USAGE: prog arg1"
        exit $ERR_NARGS
fi

#Si es demana help (-h)
if [ "$1" == "-h" -o "$1" = "--help" ]
then
	echo "@j02 Miguel Amoros"
	echo "USAGE: prog dir"
	exit 0
fi



#xixa
dir=$1

if [ -d $dir ]
then
        echo "$dir Es un directorio"
else
        echo "$dir No es un directorio"
fi
exit 0
					
