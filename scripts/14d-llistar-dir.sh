#! /bin/bash
#@j02 Miguel Amorós
#llistar dir
#fa un 'ls' del directori rebut
#procesa varios directorios PROG DIR[...]
#enumerant cada linia una sota el altre
#de cada cosa que imprime decir si es un regular file, dir, link o altre cosa
#----------------------------------------------------------------------------
ERR_NARGS=1
ERR_NODIR=2

#miramos que tenga un argument
if [ $# -lt 1 ]
then
        echo "ERROR: num arguments incorrecte"
        echo "USAGE: prog arg[...]"
        exit $ERR_NARGS
fi



#miramos que se aun directorio
for dir in $*
do
	if ! [ -d $dir ]
	then
		echo "ERROR!! $dir no es un directorio" >> dev/stderr
		echo "USAGE prog dir[...]" >> /dev/stderr
		exit $ERR_NODIR
	else

		#xixa
		#iterar pels elements el resultat de una ordre i enumerar-los
		llista=$(ls $dir)
		cont=1

		for element in $llista
		do
        		echo "$cont: $element"
			if [ -d $dir/$element ] #es de podar $dir/ para que pille la ruta absoluta sino no funciona sino es en el directorio activo y los directorios
			then
				echo "Es un directori"
			elif [ -f $dir/$element ]
			then
				echo "Es un regular file"
			elif [ -h $dir/$element ]
			then
				echo "Es un link"
			else
				echo "Es un altre cosa"
			fi
			cont=$((cont+1))
		done
	fi
done
exit 0





