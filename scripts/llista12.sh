#! /bin/bash
#@j02 Miguel Amorós
#12) Programa -h uid…
#    Per a cada uid mostra la informació de l̉usuari en format:
#    logid(uid) gname home shell
#-----------------------------------------------------------------------------------------------------------------------------------------
status=0
if [ $# -eq 1 -a "$1" = "-h" ]
then
	echo "mostrem la ajuda"
	echo "USAGE: prog -h uid..."
	exit $status
fi

if [ $# -eq 0 ]
then
	echo "ERROR: numero arg incorrecte"
	echo "USAGE: prog -h uid"
	status=1
fi



for uid in $*
do
	uidLine=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
	if [ $? -eq 0 ]
	then
		login=$(echo $uidLine | cut -d: -f1)
		gid=$(echo $uidLine | cut -d: -f4)
		home=$(echo $uidLine | cut -d: -f6)
		shell=$(echo $uidLine | cut -d: -f7)

		#para saber el gname, sabiendo el gid lo buscamos en el /etc/group con un grep y un recorte
		gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f3)

		echo "$login($uid) $gname $home $shell"
	else
		echo "$uid incorrecte" >> /dev/stderr
		status=2
	fi

done

exit $status
