#! /bin/bash
#Miguel Amoros j02
#7. Processar línia a línia l̉entrada estàndard, si la línia té més de 60 caràcters la mostra, si no no.
#--------------------------------------------------------------------------------------------------------
status=0

#vamos leyendo cada linea y si tiene mas de 60 chars, la mostramos
while read -r line
do
	egrep "^[^ ]{60,}" #buscamos +60 chars que no sea espacios
done

exit $status
