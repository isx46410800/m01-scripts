#! /bin/bash
#Miguel Amoros j02
#5. Mostrar línia a línia l̉entrada estàndard, retallant només els primers 50 caràcters.
#------------------------------------------------------------------------------------------

status=0
#para cada linea que leemos de STDIN, la printamos y la recortamos hasta maximo 50 caracteres
while read -r line
do
	echo $line | cut -c1-50
done
exit $status
