#! /bin/bash
#@j02 Miguel Amorós
#
#Exercici 16- validar arguments
#prog -h o --help
#prog -a arg
#prog -b -a arg
#cas1 arg // cas2 arg
#----------------------------------------------------------------------------
ERR_NARGS=1
OK=0

#miramos que tenga un argument
if [ $# -lt 1 -o $# -gt 3 ]
then
	echo "ERROR: num arguments incorrecte"
	echo "USAGE: prog arg -h / prog -a arg / prog -b -a arg"
	exit $ERR_NARGS
fi

#miramos si es help
if [ $# -eq 1 ]
then
	if [ "$1" == "-h" -o "$1" == "--help" ]
	then
		echo "@j02 Miguel Amoros"
		echo "USAGE: prog arg -h / prog -a arg / prog -b -a arg"
		exit $OK
	else
		echo "ERROR: format arg incorrecte"
		echo "USAGE: prog arg -h / prog -a arg / prog -b -a arg"
		exit $ERR_NARGS
	fi
fi

#Miramos si el numero de argumentos son dos
if [ $# -eq 2 ]
then
	if ! [ "$1" == "-a" ]
	then
		echo "ERROR: format args incorrecte"
		echo "USAGE: prog arg -h / prog -a arg / prog -b -a arg"
		exit $ERR_NARGS
	fi
fi

#Miramos si el numero de argumentos son tres
if [ $# -eq 3 ]
then
	if ! [ "$1" == "-b" -a "$2" == "-a" ]
	then
		echo "ERROR: format args incorrecte"
		echo "USAGE: prog arg -h / prog -a arg / prog -b -a arg"
		exit $ERR_NARGS
        fi
fi

#xixa
if [ $# -eq 2 ]
then
	echo "Cas1: argument val $2"
else
	echo "Cas2: argument val $3"
fi



