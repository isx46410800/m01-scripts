#! /bin/bash
#@j02 Miguel Amorós
#7) Programa: prog -f|-d arg1 arg2 arg3 arg4
#a) Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
#   Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis.
#   Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2.
#b) Ampliar amb el cas: prog -h|--help.

#---------------------------------------------------
OK=0
ERR_NARGS=1
ERR_ELEM=2



#numero de arguments diferente de 5 error
if [ $# -nq 5 ]
then
	echo "ERROR: Numero de args incorrecte"
	echo "USAGE: Programa: prog -f|-d arg1 arg2 arg3 arg4"
	exit $ERR_NARGS
fi

#si $1 es diferente de -d y -f
if [ "$1" != "-d" -a "$1" != "-f" ]
then
	echo "ERROR: entrada incorrecte"
	echo "USAGE: USAGE: Programa: prog -f|-d arg1 arg2 arg3 arg4"
	exit $ERR_NARGS
fi


#miramos si es help
if [ $# -eq 1 ]
then
       if [ "$1" == "-h" -o "$1" == "--help" ]
       then
             echo "@j02 Miguel Amoros"
             echo "USAGE: Programa: prog -f|-d arg1 arg2 arg3 arg4"
             exit $OK
        else
	     echo "ERROR: format arg incorrecte"
             echo "USAGE: Programa: prog -f|-d arg1 arg2 arg3 arg4"
             exit $ERR_NARGS
      fi
fi


#cogemos solo los argumentos, recortando la opcion
llista=$(echo $* | cut -d' ' -f2-5)

#miramos los argumentos
echo $llista

#si la opcion es -f
if [ "$1" = "-f" ]
then
	#para cada argumento de la lista recortada
	for argument in $llista
	do
		if ! [ -f $argument ]
		then
			echo "$argument No es un regular file"
			OK=$ERR_ELEM #si no es, el status level cambia
		else
			echo "$argument Es un regular file"
		fi
	done
fi

#si la opcion es -d
if [ "$1" = "-d" ]
then
        for argument in $llista
        do
                if ! [ -d $argument ]
                then
			echo "$argument No es un directori"
                        OK=$ERR_ELEM
		else
			echo "$argument Es un directori"
                fi
        done
	fi
echo $OK #printamos el status level
exit $OK																


