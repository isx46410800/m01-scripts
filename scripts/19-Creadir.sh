#! /bin/bash
# Miguel Amoros @j02
#Exercici 19
# Creadir.sh noudir[...] - crear de cada argument un directori amb aquell nom
#validar minim 1 arg
#no genera sortida l'ordre mkdir
#si no es pot crear, fem un STDERR
#status -> 0 tot ok // 1- erros args 2-si algun error al crear (printar aquestes opcions)
#-----------------------------------------------------------------
ERR_NARGS=1
ERR_MKDIR=2
ok=0

#validem que hi hagi minim un argument
if [ $# -lt 1 ]
then
	echo "Error: numero de arguments incorrecte"
	echo "USAGE: prog noudir[...]"
	exit $ERR_NARGS
fi

#iterem cada un dels arguments amb bucle for
for nomdir in $*
do
	mkdir $nomdir &> /dev/null #qualsevol tipos d'error, al dev null
	if [ $? -ne 0 ] #si el status level no es igual a 0:
	then
		echo "ERROR: no es crea el $nomdir" >> /dev/stderr #missatge d'error
		ok=$ERR_MKDIR #indiquem que al veure un error, el status level no es 0, es 2
	fi
done
exit $ok



