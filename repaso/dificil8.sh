#! /bin/bash
#8) Programa: prog file…
#   a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s’ha comprimit correctament, o un missatge d’error per stderror si no s’ha pogut comprimir. En finalitzar es mostra per stdout quants files ha comprimit. 
#     Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
#   b) Ampliar amb el cas: prog -h|--help.
#-------------------------------

status=0
ERR_NARGS=1
ERR_ERR=2
cont=0

if [ $# -eq 1 -a "$1" = "-h" -o "$1" = "--help" ]
then
	echo "J02 Miguel amoros"
	echo "usage: Programa: prog -f|-d arg1 arg2 arg3 arg4"
	exit $status
fi

if [ $# -lt 1 ]
then
	echo "ERR: num args incorrecte"
	echo "USAGE: Programa: prog file..."
	status=1
	exit $status
fi

for file in $*
do
	gzip $file > /dev/null
	
	if [ $? -eq 0 ]
	then
		echo $file
		cont=$((cont+1))
	else
		echo "$file error compresion"
		status=2
	fi
done

echo "se han comprimido $cont files"

exit $status


