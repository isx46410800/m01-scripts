#! /bin/bash
#@j02 Miguel Amoros
#HISX
#2. Mostar els arguments rebuts línia a línia, tot numerànt-los.
#----------------------------------------------------------
status=0

if [ $# -lt 1 ]
then
	echo "ERROR: numero de args incorrecte"
	echo "Usage: prog arg[...]"
	status=1
	exit $status
fi

cont=1

for arg in $*
do	
	echo "$cont: $arg"
	cont=$((cont+1))
done

exit $status
