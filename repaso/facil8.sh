#! /bin/bash
#@j02 Miguel Amoros
#HISX
#8. Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema 
#(en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.
#--------------------------------------------------------------------------
status=0

if [ $# -lt 1 ]
then
	echo "ERR: num args incorrecte"
	echo "USAGE: prog arg[...]"
	exit $ERR_NARGS
fi

for nom in $*
do
	egrep "^$nom:" /etc/passwd > /dev/null
	
	if ! [ $? -eq 0 ]
	then
		echo "ERROR: el $nom no pertenece a nom de usuari del sistema" >> /dev/stderr
		status=2
	else
		echo "$nom"
	fi
done

exit $status
