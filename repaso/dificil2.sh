#! /bin/bash
#j02 miguel amoros
#2) Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
#---------------------------------------------
cont=0

if [ $# -lt 1 ]
then
	echo "ERROR: num arg incorrecte"
	echo "USAGE: prog arg[...]"
	exit 1
fi

for arg in $*
do
	echo "$arg" | egrep "^.{3,}" > /dev/null
	
	if [ $? -eq 0 ]
	then
		cont=$((cont+1))
	fi
done
echo "hay $cont de arguments con 3 o mas chars"
exit 0
