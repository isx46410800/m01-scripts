#! /bin/bash
#j02 miguel amoros
#1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#---------------------------------------------

if [ $# -lt 1 ]
then
	echo "ERROR: num arg incorrecte"
	echo "USAGE: prog arg[...]"
	exit 1
fi

for arg in $*
do
	echo $arg | egrep "^.{4,}"
done

exit 0
