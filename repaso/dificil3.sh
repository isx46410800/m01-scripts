#! /bin/bash
#j02 miguel amoros
#3) Processar arguments que són matricules: 
# a) llistar les vàlides, del tipus: 9999 AAA. 
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d’errors (de no vàlides)
#---------------------------------------------
status=0
cont=0

if [ $# -lt 1 ]
then
	echo "ERROR: num arg incorrecte"
	echo "USAGE: prog arg[...]"
	status=1
	exit $status
fi

for matricula in $*
do
	echo "$matricula" | egrep "^[0-9]{4}-[A-Z]{3}$"
	
	if ! [ $? -eq 0 ]
	then
		echo "$matricula es una matricula no valida" >> /dev/stderr
		cont=$((cont+1))
		status=2
	fi
done
echo "hay $cont erronees"
exit $status
