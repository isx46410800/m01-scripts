#! /bin/bash
#@j02 Miguel Amoros
#HISX
#Exemple-1: dir si és jove, actiu, jubilat. processar n edats rebudes com a arguments.
#-------------------------------
status=0
ERR_NARGS=1

if [ $# -lt 1 ]
then
	echo "ERR: num args incorrecte"
	echo "USAGE: prog arg[...]"
	exit $ERR_NARGS
fi

for edat in $*
do
	if [ $edat -lt 18 ]
	then
		echo "$edat es una edad de jove"
	elif [ $edat -lt 65 ]
	then
		echo "$edat es una edad de actiu"
	else
		echo "$edat es una edad de jubilat"
	fi
done

exit $status
