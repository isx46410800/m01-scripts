#! /bin/bash
#j02 miguel amoros
#3) Processar arguments que són matricules: 
# a) llistar les vàlides, del tipus: 9999 AAA. 
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d’errors (de no vàlides)
#---------------------------------------------
#4) Processar stdin cmostrant per stdout les línies numerades i en majúscules..
cont=1

while read -r line
do
	echo "$cont: $line" | tr '[a-z]' '[A-Z]'
	cont=$((cont+1))
done

exit 0
