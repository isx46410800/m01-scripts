#! /bin/bash
#@j02 Miguel Amoros
#HISX
#6. Fer un programa que rep com a arguments noms de dies de la setmana i mostra
#quants dies eren laborables i quants festius. Si l’argument no és un dia de la setmana
#genera un error per stderr.
#   Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
#--------------------------------------------------------------------------
ERR_NARGS=1
ok=0

if [ $# -lt 1 ]
then
	echo "ERR: num args incorrecte"
	echo "USAGE: prog arg[...]"
	exit $ERR_NARGS
fi

cont_laborales=0
cont_festivos=0

for dia in $*
do
	case $dia in
		'lunes'|'martes'|'miercoles'|'jueves'|'viernes')
			cont_laborales=$((cont_laborales+1));;
		'sabado'|'domingo')
			cont_festivos=$((cont_festivos+1));;
		*)
			echo "ERROR: $dia no es un dia de la semana correcto" >> /dev/stderr;;
	esac
			
done
echo "hay $cont_laborales dias laborables"
echo "Hay $cont_festivos dias festivos"

exit $ok
