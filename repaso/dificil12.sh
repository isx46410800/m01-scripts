#! /bin/bash
#12) Programa -h uid…
#    Per a cada uid mostra la informació de l’usuari en format:
#    login(uid) gname home shell
#-------------------

if [ $# -lt 2 ]
then
	echo "err num args"
	echo "usage: prog -h uid"
	exit 1
fi

if [ "$1" != "-h" ]
then 
	echo "err format incorrecte"
	echo "usage: prog -h uid"
	exit 1
fi

shift

for uid in $*
do
	uidLine=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
	
	if [ $? -eq 0 ]
	then
		login=$(echo "$uidLine" | cut -d: -f1)
		home=$(echo "$uidLine" | cut -d: -f6)
		shell=$(echo "$uidLine" | cut -d: -f7)
		
		gid=$(echo $uidLine | cut -d: -f4)
		
		gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f3)
		
		echo "$login($uid) $gname $home $shell"
	
	else
		echo "$uid no es un uid del sistema" >> /dev/stderr
	fi	
		
done
exit 0
	
	
