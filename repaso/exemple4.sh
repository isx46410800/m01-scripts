#! /bin/bash
#@j02 Miguel Amoros
#HISX
#Exemple-4: processar noms per stdin, si són regular file els anem comptant, si no mostrar
missatge per stderr. Mostra quants regular files hi ha hagut.
#-------------------------------

cont=0

while read -r nom
do
	if [ -f $nom ]
	then
		cont=$((cont+1))
	else
		echo "error: $nom no es un regular file" >> /dev/stderr
	fi
	
done

echo "Hi ha $cont regulars files"
exit 0
