#! /bin/bash
#@j02 Miguel Amoros
#HISX
#9. Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el sistema 
#(en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.
#-------------------------------
status=0


while read -r nom
do	
	egrep "^$nom:" /etc/passwd > /dev/null
	if ! [ $? -eq 0 ]
	then
		echo "ERROR: el $nom no pertenece a nom de usuari del sistema" >> /dev/stderr
		status=2
	else
		echo "$nom"
	fi
done

exit $status
