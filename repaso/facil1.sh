#! /bin/bash
#@j02 Miguel Amoros
#HISX
#1. Mostrar l’entrada estàndard numerant línia a línia
#----------------------------------------------------------
status=0
cont=1

while read -r line
do
		echo "$cont: $line"
		cont=$((cont+1))
done

exit $status
