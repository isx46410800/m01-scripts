#! /bin/bash
#@j02 Miguel Amoros
#HISX
#7. Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la mostra, 
#si no no.
#--------------------------------------------------------------------------

while read -r line
do
	egrep "^[^ ]{60,}"
done

exit 0
