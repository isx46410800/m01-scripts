#! /bin/bash
#@j02 Miguel Amoros
#HISX
#5. Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
#--------------------------------------------------------------------------
status=0

while read -r line
do
	echo "$line" | cut -c1-50
done

exit $status
