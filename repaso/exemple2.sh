#! /bin/bash
#@j02 Miguel Amoros
#HISX
#Exemple-2: processar stdin login i mostrar login, uid i gid de cada un.
#-------------------------------
status=0
ERR_NARGS=1

while read -r login
do
	loginLine=$(egrep "^$login:" /etc/passwd )
	if ! [ $? -eq 0 ]
	then
		echo "ERROR, $login no es un login del sistema" >> /dev/stderr
		status=2
	else
		uid=$(echo $loginLine | cut -d: -f3)
		gid=$(echo $loginLine | cut -d: -f4)
		
		echo "login: $login, uid: $uid y gid: $gid"
	fi
	
done

exit 0
