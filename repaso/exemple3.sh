#! /bin/bash
#@j02 Miguel Amoros
#HISX
#Exemple-3: processar gnames rebuts per arguments i mostrar per a cada un el llistat
#dels usuaris del passwd que pertanyen a grup.
#-------------------------------
status=0
ERR_NARGS=1

if [ $# -lt 1 ]
then
	echo "ERR: num args incorrecte"
	echo "USAGE: prog arg[...]"
	exit $ERR_NARGS
fi


for gname in $*
do
	gnameLine=$(egrep "^$gname:" /etc/group)
	
	
	if [ $? -eq 0 ]
	then
		gid=$(echo "$gnameLine" | cut -d: -f3)
		egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd
	else
		echo "$gname no existeix en el sistema" >> /dev/stderr
		status=2
	fi
	
done

exit $status
