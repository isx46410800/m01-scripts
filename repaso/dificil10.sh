#! /bin/bash
#10) Programa: prog.sh 
#    Rep per stdin GIDs i llista per stdout la informació de cada un d’aquests grups,
#     en format: gname: GNAME, gid: GID, users: USERS
#-------------------

while read -r gid
do
	gidLine=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group) 
	
	if [ $? -eq 0 ]
	then
		gname=$(echo "$gidLine" | cut -d: -f1)
		users=$(echo "$gidLine" | cut -d: -f4)
		
		echo "gname: $gname, gid: $gid, users: $users"
	else
		echo "aquest $gid no es un gid del sistema" >> /dev/stderr
	fi

		
done

exit 0
