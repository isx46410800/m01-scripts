#! /bin/bash
#j02 miguel amoros
#6) Processar per stdin linies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.
#---------------------------------------------

while read -r line
do
	nombre=$(echo "$line" | cut -c1)
	apellido=$(echo "$line" | tr ' ' ':' | cut -d: -f2)
	echo "$nombre. $apellido"
done

exit 0
