#! /bin/bash
#9) Programa: prog.sh [ -r -m -c cognom  -j  -e edat ]  arg…
#   Escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors corresponents.
#   No cal validar ni mostrar res!
#   Per exemple si es crida: $ prog.sh -e 18 -r -c puig -j wheel postgres ldap
#   retona:     opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap»
#----------

if [ $# -lt 1 ]
then
	echo "ERR ARGS"
	echo "PROG ...."
	exit 1
fi

opciones=""
argumentos=""
edad=""
apellidos=""

while [ -n "$1" ]
do
	case $1 in
		'-r'|'-m'|'-j')
				opciones="$opciones $1";;
		'-c')
				apellidos=$2
				shift;;
		'-e')
				edad=$2
				shift;;
		*)
				argumentos="$argumentos $1";;
	esac
	shift

done
echo "las opciones son $opciones"
echo "los argumentos son $argumentos"
echo "las edades son $edad"
echo "los apellidos son $apellidos"

exit 0
