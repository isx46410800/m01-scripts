#! /bin/bash
#@j02 Miguel Amoros
#HISX
#10. Fer un programa que rep com a argument un número indicatiu del número màxim de línies a 
#mostrar. El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
#-------------------------------

cont=1
maxim=$1
while read -r line && [ $cont -le $maxim ]
do	
			echo "$cont: $line"
			cont=$((cont+1))
		
done

exit 0
