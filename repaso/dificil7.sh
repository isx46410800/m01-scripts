#! /bin/bash
#7) Programa: prog -f|-d arg1 arg2 arg3 arg4
# a) Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
#    Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis. 
#    Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2. 
# b) Ampliar amb el cas: prog -h|--help.
#--------------------------------------------------------

status=0
ERR_NARGS=1
ERR_ERR=2
if [ $# -eq 1 -a "$1" = "-h" -o "$1" = "--help" ]
then
	echo "J02 Miguel amoros"
	echo "usage: Programa: prog -f|-d arg1 arg2 arg3 arg4"
	exit $status
fi

if [ $# -ne 5 ]
then
	echo "ERR: num args incorrecte"
	echo "USAGE: Programa: prog -f|-d arg1 arg2 arg3 arg4"
	status=1
	exit $status
fi

if ! [ "$1" = "-f" -o "$1" = "-d" ]
then
	echo "ERR: format incorrecte"
	echo "Programa: prog -f|-d arg1 arg2 arg3 arg4"
	exit $status
fi

tipus="$1"
shift

for arg in $*
do
	if ! [ $tipus "$arg" ]
	then
		status=2
	fi
		
done

echo "$status"
exit $status
