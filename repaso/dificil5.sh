#! /bin/bash
#j02 miguel amoros
#5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#---------------------------------------------

while read -r line
do
	echo $line | egrep "^[a-z]{,49}" > /dev/null
	
	if [ $? -eq 0 ]
	then
		echo "aquesta $line tiene -50chs"
	fi
done

exit 0
