#! /bin/bash
#@j02 Miguel Amoros
#HISX
#3. Fer un comptador des de zero fins al valor indicat per l’argument rebut.
#--------------------------------------------------------------------------
status=0

if [ $# -ne 1 ]
then
	echo "ERROR: numero de args incorrecte"
	echo "Usage: prog arg"
	status=1
	exit $status
fi

cont=0
maxim=$1

while [ $cont -le $maxim ]
do
	echo $cont
	cont=$((cont+1))
done

exit $status
