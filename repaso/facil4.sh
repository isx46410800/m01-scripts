#! /bin/bash
#@j02 Miguel Amoros
#HISX
#4. Fer un programa que rep com a arguments números de més (un o més) i 
#indica per a cada mes rebut quants dies té el més.
#--------------------------------------------------------------------------
OK=0
ERR_NARGS=1
ERR_LEVEL=2

if [ $# -lt 1 ]
then
	echo "ERROR: numero de args incorrecte"
	echo "USAGE: entrada valida del numero 1 al 12 inclosos"
	echo "USAGE: prog arg[...]"
	exit $ERR_NARGS
fi

for mes in $*
do
	case $mes in
		'1'|'3'|'5'|'7'|'8'|'10'|'12')
			echo "El mes $mes tiene 31 dias";;
		'2')
			echo "el mes $mes tiene 28 dias";;
		'4'|'6'|'9'|'11')
			echo "El mes $mes tiene 30 dias";;
		*)
				echo "ERROR: el mes $mes es una dada no valida" >> /dev/stderr
				;;
	esac
done

exit $OK
